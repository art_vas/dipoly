/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Mon May 20 15:42:59 2013
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDockWidget>
#include <QtGui/QGraphicsView>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTextBrowser>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_2;
    QGraphicsView *boardView;
    QHBoxLayout *horizontalLayout;
    QPushButton *restartButton;
    QSpacerItem *horizontalSpacer;
    QPushButton *cmd_buy;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *cmd_build;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *cmd_bribe;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *cmd_push;
    QLineEdit *pushParam;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *cmd_next;
    QDockWidget *players;
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout_3;
    QDockWidget *dockWidget;
    QWidget *notifications;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout;
    QTextBrowser *text;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(921, 654);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(0, 0));
        MainWindow->setMaximumSize(QSize(16777215, 16777215));
        QPalette palette;
        QBrush brush(QColor(100, 100, 100, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        QBrush brush1(QColor(255, 255, 255, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        QBrush brush2(QColor(139, 139, 139, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush2);
        MainWindow->setPalette(palette);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout_4 = new QVBoxLayout(centralWidget);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        boardView = new QGraphicsView(centralWidget);
        boardView->setObjectName(QString::fromUtf8("boardView"));
        boardView->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(boardView->sizePolicy().hasHeightForWidth());
        boardView->setSizePolicy(sizePolicy1);
        boardView->setMinimumSize(QSize(600, 600));
        boardView->setMaximumSize(QSize(600, 600));
        boardView->setBaseSize(QSize(600, 600));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush1);
        QBrush brush3(QColor(74, 74, 74, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush3);
        QBrush brush4(QColor(111, 111, 111, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush4);
        QBrush brush5(QColor(92, 92, 92, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush5);
        QBrush brush6(QColor(37, 37, 37, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush6);
        QBrush brush7(QColor(49, 49, 49, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush7);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush1);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush2);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush3);
        QBrush brush8(QColor(0, 0, 0, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Shadow, brush8);
        palette1.setBrush(QPalette::Active, QPalette::AlternateBase, brush6);
        QBrush brush9(QColor(255, 255, 220, 255));
        brush9.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipBase, brush9);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipText, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush4);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush5);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Shadow, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush9);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush5);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush7);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Shadow, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush8);
        boardView->setPalette(palette1);
        boardView->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        boardView->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);

        horizontalLayout_2->addWidget(boardView);


        verticalLayout_4->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        restartButton = new QPushButton(centralWidget);
        restartButton->setObjectName(QString::fromUtf8("restartButton"));
        QFont font;
        font.setFamily(QString::fromUtf8("Arial"));
        font.setBold(true);
        font.setWeight(75);
        restartButton->setFont(font);

        horizontalLayout->addWidget(restartButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        cmd_buy = new QPushButton(centralWidget);
        cmd_buy->setObjectName(QString::fromUtf8("cmd_buy"));
        QPalette palette2;
        QBrush brush10(QColor(179, 179, 179, 255));
        brush10.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::Button, brush10);
        QBrush brush11(QColor(17, 10, 218, 255));
        brush11.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush11);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush10);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush11);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush10);
        QBrush brush12(QColor(106, 104, 100, 255));
        brush12.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush12);
        cmd_buy->setPalette(palette2);
        QFont font1;
        font1.setFamily(QString::fromUtf8("Arial"));
        font1.setBold(true);
        font1.setItalic(false);
        font1.setWeight(75);
        cmd_buy->setFont(font1);

        horizontalLayout->addWidget(cmd_buy);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        cmd_build = new QPushButton(centralWidget);
        cmd_build->setObjectName(QString::fromUtf8("cmd_build"));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::Button, brush10);
        QBrush brush13(QColor(191, 63, 191, 255));
        brush13.setStyle(Qt::SolidPattern);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush13);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush10);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush13);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush10);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush12);
        cmd_build->setPalette(palette3);
        cmd_build->setFont(font);

        horizontalLayout->addWidget(cmd_build);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        cmd_bribe = new QPushButton(centralWidget);
        cmd_bribe->setObjectName(QString::fromUtf8("cmd_bribe"));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::Button, brush10);
        QBrush brush14(QColor(0, 170, 93, 255));
        brush14.setStyle(Qt::SolidPattern);
        palette4.setBrush(QPalette::Active, QPalette::ButtonText, brush14);
        palette4.setBrush(QPalette::Inactive, QPalette::Button, brush10);
        palette4.setBrush(QPalette::Inactive, QPalette::ButtonText, brush14);
        palette4.setBrush(QPalette::Disabled, QPalette::Button, brush10);
        palette4.setBrush(QPalette::Disabled, QPalette::ButtonText, brush12);
        cmd_bribe->setPalette(palette4);
        cmd_bribe->setFont(font1);

        horizontalLayout->addWidget(cmd_bribe);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        cmd_push = new QPushButton(centralWidget);
        cmd_push->setObjectName(QString::fromUtf8("cmd_push"));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::Button, brush10);
        palette5.setBrush(QPalette::Active, QPalette::ButtonText, brush8);
        palette5.setBrush(QPalette::Inactive, QPalette::Button, brush10);
        palette5.setBrush(QPalette::Inactive, QPalette::ButtonText, brush8);
        palette5.setBrush(QPalette::Disabled, QPalette::Button, brush10);
        palette5.setBrush(QPalette::Disabled, QPalette::ButtonText, brush12);
        cmd_push->setPalette(palette5);
        cmd_push->setFont(font);

        horizontalLayout->addWidget(cmd_push);

        pushParam = new QLineEdit(centralWidget);
        pushParam->setObjectName(QString::fromUtf8("pushParam"));
        pushParam->setMaximumSize(QSize(86, 16777215));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Kristen ITC"));
        font2.setBold(true);
        font2.setWeight(75);
        pushParam->setFont(font2);

        horizontalLayout->addWidget(pushParam);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_7);

        cmd_next = new QPushButton(centralWidget);
        cmd_next->setObjectName(QString::fromUtf8("cmd_next"));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::Button, brush10);
        QBrush brush15(QColor(255, 0, 0, 255));
        brush15.setStyle(Qt::SolidPattern);
        palette6.setBrush(QPalette::Active, QPalette::ButtonText, brush15);
        palette6.setBrush(QPalette::Inactive, QPalette::Button, brush10);
        palette6.setBrush(QPalette::Inactive, QPalette::ButtonText, brush15);
        palette6.setBrush(QPalette::Disabled, QPalette::Button, brush10);
        palette6.setBrush(QPalette::Disabled, QPalette::ButtonText, brush12);
        cmd_next->setPalette(palette6);
        cmd_next->setFont(font);

        horizontalLayout->addWidget(cmd_next);


        verticalLayout_4->addLayout(horizontalLayout);

        MainWindow->setCentralWidget(centralWidget);
        players = new QDockWidget(MainWindow);
        players->setObjectName(QString::fromUtf8("players"));
        players->setMinimumSize(QSize(300, 300));
        QPalette palette7;
        QBrush brush16(QColor(189, 189, 189, 255));
        brush16.setStyle(Qt::SolidPattern);
        palette7.setBrush(QPalette::Active, QPalette::Text, brush16);
        palette7.setBrush(QPalette::Inactive, QPalette::Text, brush16);
        palette7.setBrush(QPalette::Disabled, QPalette::Text, brush12);
        players->setPalette(palette7);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QString::fromUtf8("dockWidgetContents"));
        verticalLayout_3 = new QVBoxLayout(dockWidgetContents);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        players->setWidget(dockWidgetContents);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), players);
        dockWidget = new QDockWidget(MainWindow);
        dockWidget->setObjectName(QString::fromUtf8("dockWidget"));
        dockWidget->setMinimumSize(QSize(300, 350));
        dockWidget->setMaximumSize(QSize(524287, 350));
        notifications = new QWidget();
        notifications->setObjectName(QString::fromUtf8("notifications"));
        verticalLayoutWidget_2 = new QWidget(notifications);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(10, 10, 281, 311));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        text = new QTextBrowser(verticalLayoutWidget_2);
        text->setObjectName(QString::fromUtf8("text"));
        QPalette palette8;
        QBrush brush17(QColor(176, 176, 176, 255));
        brush17.setStyle(Qt::SolidPattern);
        palette8.setBrush(QPalette::Active, QPalette::Base, brush17);
        palette8.setBrush(QPalette::Inactive, QPalette::Base, brush17);
        palette8.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        text->setPalette(palette8);
        QFont font3;
        font3.setFamily(QString::fromUtf8("Arial"));
        text->setFont(font3);

        verticalLayout->addWidget(text);

        dockWidget->setWidget(notifications);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), dockWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Dipoly", 0, QApplication::UnicodeUTF8));
        restartButton->setText(QApplication::translate("MainWindow", "Restart Game", 0, QApplication::UnicodeUTF8));
        cmd_buy->setText(QApplication::translate("MainWindow", "Buy", 0, QApplication::UnicodeUTF8));
        cmd_build->setText(QApplication::translate("MainWindow", "Build", 0, QApplication::UnicodeUTF8));
        cmd_bribe->setText(QApplication::translate("MainWindow", "Bribe", 0, QApplication::UnicodeUTF8));
        cmd_push->setText(QApplication::translate("MainWindow", "Push", 0, QApplication::UnicodeUTF8));
        cmd_next->setText(QApplication::translate("MainWindow", "Next", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
