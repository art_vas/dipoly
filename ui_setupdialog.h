/********************************************************************************
** Form generated from reading UI file 'setupdialog.ui'
**
** Created: Mon May 20 15:42:59 2013
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETUPDIALOG_H
#define UI_SETUPDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SetupDialog
{
public:
    QPushButton *start;
    QGroupBox *init;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLineEdit *fileNameBox;
    QSpacerItem *horizontalSpacer;
    QPushButton *loadBoard;
    QGroupBox *players;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_4;

    void setupUi(QDialog *SetupDialog)
    {
        if (SetupDialog->objectName().isEmpty())
            SetupDialog->setObjectName(QString::fromUtf8("SetupDialog"));
        SetupDialog->resize(470, 392);
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        QBrush brush1(QColor(168, 168, 168, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        SetupDialog->setPalette(palette);
        SetupDialog->setInputMethodHints(Qt::ImhNone);
        start = new QPushButton(SetupDialog);
        start->setObjectName(QString::fromUtf8("start"));
        start->setGeometry(QRect(380, 360, 81, 23));
        QPalette palette1;
        QBrush brush2(QColor(131, 129, 124, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        start->setPalette(palette1);
        QFont font;
        font.setFamily(QString::fromUtf8("Arial"));
        font.setBold(true);
        font.setWeight(75);
        start->setFont(font);
        start->setFocusPolicy(Qt::NoFocus);
        init = new QGroupBox(SetupDialog);
        init->setObjectName(QString::fromUtf8("init"));
        init->setGeometry(QRect(10, 10, 451, 62));
        init->setFont(font);
        verticalLayout = new QVBoxLayout(init);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        fileNameBox = new QLineEdit(init);
        fileNameBox->setObjectName(QString::fromUtf8("fileNameBox"));

        horizontalLayout->addWidget(fileNameBox);

        horizontalSpacer = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        loadBoard = new QPushButton(init);
        loadBoard->setObjectName(QString::fromUtf8("loadBoard"));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::Button, brush2);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        loadBoard->setPalette(palette2);
        loadBoard->setFont(font);
        loadBoard->setFocusPolicy(Qt::NoFocus);

        horizontalLayout->addWidget(loadBoard);


        verticalLayout->addLayout(horizontalLayout);

        players = new QGroupBox(SetupDialog);
        players->setObjectName(QString::fromUtf8("players"));
        players->setGeometry(QRect(10, 100, 391, 241));
        players->setFont(font);
        verticalLayout_5 = new QVBoxLayout(players);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));

        verticalLayout_5->addLayout(verticalLayout_4);


        retranslateUi(SetupDialog);

        QMetaObject::connectSlotsByName(SetupDialog);
    } // setupUi

    void retranslateUi(QDialog *SetupDialog)
    {
        SetupDialog->setWindowTitle(QApplication::translate("SetupDialog", "Dipoly Setup", 0, QApplication::UnicodeUTF8));
        start->setText(QApplication::translate("SetupDialog", "Start", 0, QApplication::UnicodeUTF8));
        init->setTitle(QApplication::translate("SetupDialog", "Initialization ", 0, QApplication::UnicodeUTF8));
        loadBoard->setText(QApplication::translate("SetupDialog", "Load Init File ...", 0, QApplication::UnicodeUTF8));
        players->setTitle(QApplication::translate("SetupDialog", "Players", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SetupDialog: public Ui_SetupDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETUPDIALOG_H
