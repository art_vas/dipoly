﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace initReader
{
   // Wrapper class for the generated xml reader class
   // See the style 
   class Square
   {
      public Square(string n, string t, int sprice, int shackP, int shackA)
      {
         name = n;
         type = t;
         price = sprice;
         shackPrice = shackP;
         shackAmount = shackA;

      }
      // some (ugly) properties
      public string name { get; set; }
      public string type { get; set; }
      public int price { get; set; }
      public int shackPrice { get; set; }
      public int shackAmount{ get; set; }
   }
   class Card
   {
      public Card(string desc, int cmoney, int cmove, bool cprison )
      {
         description = desc;
         money = cmoney;
         move = cmove;
         prison = cprison;
      }
      public string description { get; set; }
      public int money { get; set; }
      public int move { get; set; }
      public bool prison { get; set; }
   }

   class InitfileReader
   {
      private List<Square> squares = new List<Square>();
      private List<Card> cards = new List<Card>();
      static private List<string> validation = new List<string>();

      // no getter functions, using properties instead
      public List<Card> Cards { get { return cards; } }
      public List<Square> Squares { get { return squares; } }
      public List<string> Validation { get { return validation; } } 
      public int Seedvalue { get; set; }
      public int StartingMoney { get; set; }
      public string Name { get; set; }

      // handle the validator events and add them to list 
      static void validaatoriEventHandler(object lahettaja, ValidationEventArgs args)
      {
         if (args.Severity == XmlSeverityType.Error)
         {
            validation.Add("Error in the initialization files: " + args.Message);
         }
         else
         {
            validation.Add("Error: No XML schema found(?): " + args.Message);
         }
      }

      // Reads the given initialization file, uses validator to validate
      // the file against the given XML schema. Stores the data.
      public void readFile(string filename)
      {
         // validation first
         XmlReaderSettings asetukset = new XmlReaderSettings();
         asetukset.ValidationType = ValidationType.Schema;
         asetukset.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
         asetukset.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
         asetukset.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
         asetukset.ValidationEventHandler += new ValidationEventHandler(validaatoriEventHandler);
         XmlReader ifreader = XmlReader.Create(filename, asetukset);
         while (ifreader.Read());

         // Here you could check if there are errors that break the schema & stop reading
         // if so, but we continue instead

         // Reading the xml-file again and now trying to understand the contents.
         TextReader textReader = new StreamReader(filename);
         XmlSerializer xmlHelper = new XmlSerializer(typeof(initfile));
         initfile initf = (initfile)xmlHelper.Deserialize(textReader);

         initfileSquare[] isquares = initf.square;
         foreach (initfileSquare k in isquares )
         {
            Square r = new Square(k.name, k.type, k.price, k.shackprice, k.shackamount);
            Squares.Add(r);
         }


         initfileCard[] icards = initf.card;
         foreach (initfileCard ak in icards)
         {
            
            Card newcard = new Card(ak.description, ak.money, ak.move, ak.prison);
            Cards.Add(newcard);
         }
         Name = initf.name;
         StartingMoney = initf.startmoney;
         Seedvalue = initf.seed;
      }

   }

}
