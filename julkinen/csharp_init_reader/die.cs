﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace initReader
{
   class Die
   {
      Random random;
      const int FACES = 6;
      int last_casted = 0;
      
      public Die(int seedvalue)
      {
         if (seedvalue < 1)
         {
            random = new Random();
         }
         else
         {
            random = new Random(seedvalue);
         }
      }
      public Die(): this(0)
      {
      }

      // cast the die
      public int cast()
      {
         last_casted = random.Next(FACES);
         return last_casted;
      }

      // returns the result of the last cast 
      public int getLastCasted()
      {
         return last_casted;
      }
   }
}
