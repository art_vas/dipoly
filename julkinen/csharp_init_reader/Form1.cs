﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace initReader
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}
		private initReader.InitfileReader reader = null;

		private void button1_Click(object sender, EventArgs e)
		{
			openFileDialog1.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
			DialogResult result = openFileDialog1.ShowDialog();
         if (result == DialogResult.OK) // Test result.
         {
            string filename = openFileDialog1.FileName;
            textBox1.AppendText( "Init file name \n" + filename + "\n" );
            try
            {
               reader = new InitfileReader();
               reader.readFile(filename);
               // check the validation results
               if (reader.Validation.Count > 0)
               {
                  System.Console.WriteLine("XML violates the schema:");
                  // here you could inform the user about the failures and stop loading
                  // the game using an invalid init file
                  foreach (string s in reader.Validation)
                  {
                     textBox1.AppendText( "Invalid initialization file: " + s + "\n" );
                  }
               }

               textBox1.AppendText( "Name of the board " + reader.Name + "\nstart money: " + reader.StartingMoney +"\n");
               textBox1.AppendText("################\n");

               textBox1.AppendText( "Squares:" );

               // getting the squares and creating a new container for them just for a show
               List<Square> ruudut = reader.Squares;
               foreach (Square k in ruudut)
               {
                  textBox1.AppendText( k.name + ", vuokra: " + k.price + "\n" );
               }
               System.Console.WriteLine("\nKORTIT");

               // browsing the cards directly from the property
               foreach (Card k in reader.Cards)
               {
                  textBox1.AppendText("Card: " + k.description + "\n money:" + k.money + ", move: " + k.move + "\n" );
               }
            }
            catch (Exception ex)
            {
               textBox1.AppendText("Exception: " + ex.Message + "\n");
               
               throw ex;
            }

            // demonstration on the usage of the Die (throwing the die 20 times in a row)
            Die die = new Die(reader.Seedvalue);
            for (int i = 0; i < 20; ++i)
            {
               textBox1.AppendText("Die was cast, result: " + die.cast() + "\n");
            }
         }

		}

      private void Form1_Load(object sender, EventArgs e)
      {

      }
	}
}
