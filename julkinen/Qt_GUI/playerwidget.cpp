#include "playerwidget.h"
#include <QtWidgets/QHBoxLayout>

namespace {
static const QString ICON_PATH = "icon/player";
static const QString ICON_EXT =  ".ico";
static const QString MONEY_ICON_PATH = "icon/money.ico";
static const int ICON_SCALE = 40;
static const QString SPACING = "    ";
static const QFont FONT = QFont("MS Shell Dlg 2", 12);
}

PlayerWidget::PlayerWidget( int id, std::string name, int money ):
    id_( id ), money_( new QLabel() )
{
    QHBoxLayout* layout = new QHBoxLayout( this );
    layout->setAlignment( Qt::AlignLeft );
    this->setLayout( layout );

    // id
    QLabel* idLabel = new QLabel ( QString::number(id) + SPACING );
    idLabel->setFont( FONT );
    layout->addWidget( idLabel );

    // avatar
    avatar_ = QPixmap( ICON_PATH + QString::number( id ) + ICON_EXT,
                       0, Qt::AutoColor );
    QLabel* avatarLabel = new QLabel();
    avatarLabel->setPixmap( avatar_.scaledToWidth( ICON_SCALE ) );
    layout->addWidget( avatarLabel );

    // name
    QLabel* nameLabel = new QLabel ( QString::fromStdString( name ) + SPACING );
    nameLabel->setFont( FONT );
    layout->addWidget( nameLabel );

    // money
    QPixmap moneyPixmap = QPixmap( MONEY_ICON_PATH, 0, Qt::AutoColor );
    QLabel* moneyLabel = new QLabel();
    moneyLabel->setPixmap( moneyPixmap.scaledToWidth( ICON_SCALE ) );
    layout->addWidget ( moneyLabel );
    money_->setFont( FONT );
    money_->setText( QString::number( money ) );
    layout->addWidget( money_ );
}

PlayerWidget::~PlayerWidget()
{

}

int PlayerWidget::getId() const
{
   return id_;
}
QPixmap PlayerWidget::getAvatar() const
{
   return avatar_;
}

void PlayerWidget::setMoney( int money )
{
   money_->setText( QString::number( money ) );
}
