// the squares in the gui

#ifndef SQUAREWIDGET_H
#define SQUAREWIDGET_H

#include <QtWidgets/QGraphicsItem>
#include <QtWidgets/QGraphicsEllipseItem>
#include "playerwidget.h"


class SquareWidget : public QGraphicsItem
{
public:
    SquareWidget( std::string name, int id, int maxshack,
                  char owner, int price );
    ~SquareWidget(){}
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint( QPainter *painter, const QStyleOptionGraphicsItem *item,
                QWidget *widget );
    void setPosition( int x, int y );
    void addVisitor( PlayerWidget *visitor );
    void removeVisitor( int visitor );
    void setOwner( char owner );
    void setPrice( int price );
    void setShack( int shack );
    void changeColor( QColor color );

private:
    int x_;
    int y_;
    std::string name_;
    int id_;
    int shack_;
    int maxshack_;
    int owner_;
    int price_;
    QList<PlayerWidget*> visitors_;
    QColor color_;
    SquareWidget& operator= ( const SquareWidget& other );
    SquareWidget( const SquareWidget& other );
};

#endif // SQUAREWIDGET_H
