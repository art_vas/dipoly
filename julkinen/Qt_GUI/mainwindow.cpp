#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "logo.h"
#include "../qt_project/initdialog.hh"
#include "gameinterface.h"
#include <qmath.h>
#include <QMessageBox>

#include <iostream>
using namespace std;



namespace {
static const int BOARDWIDTH = 600;
static const int ITEMWIDTH = 120;
static const QString MSG_DIE_CAST = "The die is cast: ";
static const QString MSG_BANKRUPT = " has been bankrupted.";
static const QString MSG_WINNER =  " won.";
static const QString ERR_PUSH_PARAM = "Paramater for command Push is not valid!";
static const int PUSH_PARAM_MIN = 1;
static const int PUSH_PARAM_MAX = 5;
static const QString CURRENCY = "$";
static const QString CARD_MONEY = "money: ";
static const QString CARD_MOVE = "move: ";
static const QString CARD_PRISON = "prison: ";
static const QColor MSG_COLOR = Qt::black;
static const QColor ERR_COLOR = Qt::red;
}


QColor colorAsQColor( enum UIInterface::SquareColor color )
{
   switch( color )
   {
      case UIInterface::RED: { return Qt::red; }
      case UIInterface::BLUE: { return Qt::blue; }
      case UIInterface::BLACK: { return Qt::black; }
      case UIInterface::GREEN: { return Qt::green; }
      case UIInterface::GRAY: { return Qt::lightGray; }
      case UIInterface::WHITE: { return Qt::white; }
      case UIInterface::PINK: { return Qt::magenta; }
      case UIInterface::YELLOW: { return Qt::yellow; }
   default: return Qt::gray;
   }
}

bool MainWindow::sanityCheckForSquareID( int squareId )
{
   QMap<int, SquareWidget*>::const_iterator it = board_.find( squareId );
   if( it != board_.end() ){ return true; }
   QString temp = QString::number( squareId );
   error( "ERROR: square id incorrect: " + temp.toStdString() );
   return false;
}



MainWindow::MainWindow( GameInterface *game, QWidget *parent ) :
   QMainWindow( parent ), ui( new Ui::MainWindow ),
   game_( game ),  boardScene_( new QGraphicsScene( this ) ){

   ui->setupUi( this );
   //game->attach( this );
   this->show();
   ui->boardView->setScene( boardScene_ );
   ui->boardView->setOptimizationFlags( QGraphicsView::DontSavePainterState );
   ui->boardView->setViewportUpdateMode( QGraphicsView::SmartViewportUpdate );
   setup_ = new InitDialog( this,  game_, this );

   //connect( setup_, SIGNAL( closed() ), this, SLOT( setupGui() ) );
   connect( ui->cmd_next, SIGNAL( clicked() ), this, SLOT( nextClicked() ) );
   connect( ui->cmd_buy, SIGNAL( clicked() ), this, SLOT( buyClicked() ) );
   connect( ui->cmd_build, SIGNAL( clicked() ), this, SLOT( buildClicked() ) );
   connect( ui->cmd_bribe, SIGNAL( clicked() ), this, SLOT( bribeClicked() ) );
   connect( ui->cmd_push, SIGNAL( clicked() ), this, SLOT( pushClicked() ) );

   setup_->exec();
   setupGui();
}

MainWindow::~MainWindow(){
   delete game_;
   delete ui;
   for ( QMap<int, PlayerWidget*>::iterator i = players_.begin();
         i != players_.end(); ++i ){
      delete *i;
   }
   delete boardScene_;
   delete setup_;

}

void MainWindow::resetGame(){

   for ( QMap<int, PlayerWidget*>::iterator it = players_.begin();
         it != players_.end(); ++it ){
      ui->players->widget()->layout()->removeWidget( *it );
      delete *it;
   }
   players_.clear();
   for ( QMap<int, SquareWidget*>::iterator i = board_.begin();
         i != board_.end(); ++i ){
      delete *i;
   }
   board_.clear();
   ui->text->clear();
}

// virtual functions
void MainWindow::error( std::string err ) {
   ui->text->setTextColor( ERR_COLOR );
   ui->text->append( QString::fromStdString( err ) );
}

void MainWindow::message( std::string msg ) {
   ui->text->setTextColor( MSG_COLOR );
   ui->text->append( QString::fromStdString( msg ) );
}

void MainWindow::debugMessage( std::string debug )
{
   qDebug( debug.c_str() );
}

void MainWindow::addSquare( std::string name, int id, int shackMax,
                            char owner, int price ) {
   board_.insert( id, new SquareWidget( name, id,
                                        shackMax, owner, price ) );
}

void MainWindow::newTurn( std::string name, int id, int money, bool  ) {
   ui->text->setTextColor( MSG_COLOR );
   ui->text->append(QString::fromStdString( name ) + " " +
                    QString::number( id ) + " " +
                    QString::number( money ) + " " +
                    CURRENCY );
}

void MainWindow::rollDice( int diceValue ) {
   ui->text->setTextColor( MSG_COLOR );
   ui->text->append( MSG_DIE_CAST + QString::number( diceValue ) );
}


void MainWindow::removePlayer( int squareId, int playerId ) {
   if( !sanityCheckForSquareID( squareId ) ){ return; }
   board_[squareId]->removeVisitor( playerId );
   ui->boardView->viewport()->update();
}

void MainWindow::addPlayer( int squareId, int playerId ) {
   if( !sanityCheckForSquareID( squareId ) ){ return; }
   board_[squareId]->addVisitor( players_[playerId] );
   ui->boardView->viewport()->update();
}

void MainWindow::createPlayer( int id, std::string name, int money ){
   players_.insert( id, new PlayerWidget( id, name, money ) );

}


void MainWindow::changeOwner( int squareId, int owner ) {
   if( !sanityCheckForSquareID( squareId ) ){ return; }
   if( owner == 0 ){ board_[squareId]->setOwner( ' ' ); }
   else{ board_[squareId]->setOwner( QString::number( owner ).toStdString().at(0) ); }
   ui->boardView->viewport()->update();
}

void MainWindow::changePrice( int squareId, int price ) {
   if( !sanityCheckForSquareID( squareId ) ){ return; }
   board_[squareId]->setPrice( price );
   ui->boardView->viewport()->update();
}

void MainWindow::changeShacks( int squareId, int shackAmount, int  ) {
   if( !sanityCheckForSquareID( squareId ) ){ return; }
   board_[squareId]->setShack( shackAmount );
   ui->boardView->viewport()->update();
}

void MainWindow::changeColor(int squareId, SquareColor color ){
   if( !sanityCheckForSquareID( squareId ) ){ return; }
   board_[squareId]->changeColor( colorAsQColor( color ) );
}


void MainWindow::draw() {
   int width = sqrt( float( board_.size() ) );
   int x = 0 - BOARDWIDTH/2 + ITEMWIDTH/2;
   int y = 0 - BOARDWIDTH/2 + ITEMWIDTH/2;

   // top
   for (int i(1); i <= width; ++i ){
      board_[i]->setPosition( x, y );
      boardScene_->addItem( board_[i] );
      x = x + ITEMWIDTH;
   }

   // right
   for ( int i( width + 1 ); i <= 2 * width; ++i ){
      board_[i]->setPosition( x, y );
      boardScene_->addItem( board_[i] );
      y = y + ITEMWIDTH;
   }

   // bottom
   for ( int i( 2 * width + 1 ); i <= 3 * width; ++i ){
      board_[i]->setPosition( x, y );
      boardScene_->addItem( board_[i] );
      x = x - ITEMWIDTH;
   }

   // left
   for ( int i( 3 * width+1 ); i <= 4 * width; ++i ){
      board_[i]->setPosition( x, y );
      boardScene_->addItem( board_[i] );
      y = y - ITEMWIDTH;
   }

   // LOGO
   logo = new Logo();
   boardScene_->addItem( logo );
}

void MainWindow::announceWinner( std::string player ) {
   ui->text->append( QString::fromStdString( player ) + MSG_WINNER );
   toggleCommandActivity();
}

void MainWindow::announceBankrupt( std::string player ) {
   ui->text->append( QString::fromStdString( player ) + MSG_BANKRUPT );
}


void MainWindow::drawCard( std::string description, int money, int move,
                           int prison ) {
   ui->text->setTextColor( MSG_COLOR );
   ui->text->append( QString::fromStdString( description ));
   ui->text->append( CARD_MONEY + QString::number( money ));
   ui->text->append( CARD_MOVE + QString::number( move ));
   if(prison)
      ui->text->append( CARD_PRISON + "YES");
   else
      ui->text->append( CARD_PRISON + "NO");
}

void MainWindow::free( int playerId )
{
   ui->text->append( "Player " + QString::number( playerId ) + " no longer in prison." );
}

void MainWindow::imprison( int playerId )
{
   ui->text->append( "Player " + QString::number( playerId ) + " imprisoned." );
}

void MainWindow::changeMoney( int playerId, int money ) {

   QMap<int, PlayerWidget*>::const_iterator it = players_.find( playerId );
   if( it != players_.end() )
   {
      players_[playerId]->setMoney( money );
   }
   else
   {
      error( "ERROR: illegal player id!!!" );
   }
}

// public slots
void MainWindow::setupGui(){
   for ( QMap<int, PlayerWidget*>::iterator i = players_.begin();
         i != players_.end(); ++i ){
      ui->players->widget()->layout()->addWidget( *i );
   }
   draw();
}

void MainWindow::nextClicked(){
   game_->processCommand( CommandDefs::Command( CommandDefs::NEXT ) );
}

void MainWindow::buyClicked(){
   game_->processCommand( CommandDefs::Command( CommandDefs::BUY ) );
}

void MainWindow::buildClicked(){
   game_->processCommand( CommandDefs::Command( CommandDefs::BUILD ) );
}

void MainWindow::bribeClicked(){
   game_->processCommand( CommandDefs::Command( CommandDefs::BRIBE ) );
}

void MainWindow::pushClicked(){
   int pushParam = ui->pushParam->text().toInt();
   if ( pushParam && pushParam >= PUSH_PARAM_MIN && pushParam <= PUSH_PARAM_MAX ) {
      game_->processCommand( CommandDefs::Command( CommandDefs::PUSH, pushParam ) );
   }
   else {
      QMessageBox msgBox;
      msgBox.setText( ERR_PUSH_PARAM );
      msgBox.exec();
   }
}

void MainWindow::toggleCommandActivity( bool active ){
   ui->cmd_bribe->setEnabled(active);
   ui->cmd_build->setEnabled(active);
   ui->cmd_buy->setEnabled(active);
   ui->cmd_next->setEnabled(active);
   ui->cmd_push->setEnabled(active);
}

void MainWindow::on_restartButton_clicked()
{
   if( setup_ != 0 ){ delete setup_; }

   resetGame();
   setup_ = new InitDialog( this,  game_, this );
   setup_->exec();
   setupGui();
   toggleCommandActivity( true );
}



