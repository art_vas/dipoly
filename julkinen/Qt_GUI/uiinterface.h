// Use this interface to access/change the user interface.
// Do not touch the user interface components directly, only
// do updates through this interface

#ifndef UIINTERFACE_H
#define UIINTERFACE_H

#include <string>

class UIInterface
{
public:
   virtual ~UIInterface(){}

   // error message
   virtual void error(std::string err) = 0;

   // other message to message view of the UI
   virtual void message(std::string msg) = 0;

   // writes message to debug console (not on the normal UI)
   virtual void debugMessage(std::string msg) = 0;

   // adds a square to UI (do only once when game is initialized)
   virtual void addSquare(std::string name, int id, int shackMax,
                          char owner, int price) = 0;

   // Gives information about the starting of a new turn
   virtual void newTurn(std::string name, int id, int money, bool inPrison) = 0;

   // shows the result of the die roll
   virtual void rollDice(int diceValue) = 0;

   // Player id is from 1 to MAX_PLAYERS
   // These functions remove and add player to a square on the user interface
   virtual void removePlayer(int squareId, int playerId) = 0;
   virtual void addPlayer(int squareId, int playerId) = 0;

   // this function is used to create a new player
   virtual void createPlayer(int id, std::string name, int money) = 0;

   // These functions change the properties of the squares on the UI
   // ..square id's are from 1 to number of squares
   // ..player id ranges from 1 to max number of players
   // ..giving 0 (zero) as owner parameter removes the owner
   virtual void changeOwner(int squareId, int owner) = 0;
   virtual void changePrice(int squareId, int price) = 0;
   virtual void changeShacks(int squareId, int shackAmount, int maxAmount=0) = 0;

   enum SquareColor{ DEFAULT, BLACK, RED, BLUE, YELLOW, GREEN, WHITE, GRAY, PINK };
   // changing the color of a street
   virtual void changeColor(int squareId, SquareColor color ) = 0;

   // This function gives the information related to drawing a card
   virtual void drawCard(std::string description, int money, int move, int prison) = 0;

   // Inform a player is imprisoned or freed from prison
   virtual void free(int playerId) = 0;
   virtual void imprison(int playerId) = 0;

   virtual void changeMoney(int playerId, int money) = 0;

   // Game ending functions
   virtual void draw() = 0;
   virtual void announceWinner(std::string player) = 0;
   virtual void announceBankrupt(std::string player) = 0;

};

#endif // UIINTERFACE_H
