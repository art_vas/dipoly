// Mainwindow for the game, implements the uiinterface
// See UIInterface for the details on functions, no need to
// go through anything in here.

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include "uiinterface.h"
#include "squarewidget.h"
#include "playerwidget.h"
#include "logo.h"

namespace Ui {
class MainWindow;
}

class InitDialog;
class GameInterface;

class MainWindow : public QMainWindow, public UIInterface
{
    Q_OBJECT
    
public:
    explicit MainWindow( GameInterface* game, QWidget *parent = 0 );
    ~MainWindow();
    virtual void error( std::string err );
    virtual void message( std::string msg );
    virtual void debugMessage( std::string msg );

    virtual void addSquare( std::string name, int id, int shackMax,
                            char owner, int price );
    virtual void newTurn( std::string name, int id, int money, bool inPrison );
    virtual void rollDice( int diceValue );
    virtual void removePlayer( int squareId, int playerId );
    virtual void addPlayer( int squareId, int playerId );
    virtual void createPlayer( int id, std::string name, int money );
    virtual void changeOwner( int squareId, int owner );
    virtual void changePrice( int squareId, int price );
    virtual void changeShacks( int squareId, int shackAmount, int maxAmount=0 );
    virtual void changeColor(int squareId, SquareColor color );

    virtual void draw();
    virtual void announceWinner( std::string player );
    virtual void announceBankrupt( std::string player );
    virtual void drawCard( std::string description, int money, int move, int prison );
    virtual void free( int playerId );
    virtual void imprison( int playerId );
    virtual void changeMoney( int playerId, int money );
    void toggleCommandActivity(bool active = false);

public slots:
    void setupGui();
    void nextClicked();
    void buyClicked();
    void buildClicked();
    void bribeClicked();
    void pushClicked();

private slots:
    void on_restartButton_clicked();

private:

    MainWindow( const MainWindow& );
    MainWindow& operator =( const MainWindow& );

    void resetGame();
    bool sanityCheckForSquareID( int squareId );

    Ui::MainWindow* ui;
    InitDialog* setup_;
    GameInterface* game_;
    QGraphicsScene* boardScene_;
    QMap<int, SquareWidget*> board_;
    QMap<int, PlayerWidget*> players_;
    Logo* logo;
};

#endif // MAINWINDOW_H
