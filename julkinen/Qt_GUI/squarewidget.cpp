#include "squarewidget.h"
#include <QPainter>

namespace {
static const int WIDTH = 120;
static const int HEIGHT = 120;
static const int TEXT_WIDTH = 100;
static const int TEXT_HEIGHT = 60;
static const int RADIUS = 20;
static const int OFFSET = 10;
static const QColor FILLCOLOR = Qt::gray;
static const QColor BORDERCOLOR = Qt::black;
static const QString EMPTY = " ";
static const QString OWNER = "O:";
static const QString SHACK = "S:";
static const QString PLR_ID = "id:";
static const QString PRICE = "$:";
static const QString PLAYERS = "P:";
static const QString SEPERATOR = "/";
static const QString NEWLINE = "\n";
static const int DEFAULT_SHACK = 0;
}

SquareWidget::SquareWidget( std::string name, int id, int maxshack,
                           char owner, int price ):
    x_( 0 ), y_( 0 ),
    name_( name ), id_( id ), shack_( DEFAULT_SHACK ),
    maxshack_( maxshack ), owner_( owner ), price_( price ), visitors_(), color_( FILLCOLOR ) {}


QRectF SquareWidget::boundingRect() const {
    return QRectF( 0, 0, WIDTH, HEIGHT );
}

QPainterPath SquareWidget::shape() const {
    QPainterPath path;
    path.addRect( 0, 0, WIDTH, HEIGHT );
    return path;
}

void SquareWidget::paint( QPainter *painter,
                         const QStyleOptionGraphicsItem* , QWidget* ){

    // square
    painter->setPen( BORDERCOLOR );
    painter->setBrush( FILLCOLOR );
    painter->drawRect( QRectF( x_, y_, WIDTH, HEIGHT ) );

    // draw color marker on the square
    painter->setBrush( color_ );
    painter->setPen( color_ );
    painter->drawRect( QRectF( x_, y_+ HEIGHT-10, WIDTH, 10 ) );

    painter->setPen( BORDERCOLOR );
    painter->setBrush( FILLCOLOR );

    // text
    painter->setFont( QFont( "MS Shell Dlg 2", 12 ) );
    painter->save();
    QString text = QString::fromStdString( name_ ) + NEWLINE +
            PLR_ID + QString::number( id_ ) + EMPTY +
            SHACK + QString::number( shack_ ) + SEPERATOR +
            QString::number( maxshack_ ) + NEWLINE +
            OWNER + owner_ + EMPTY + PRICE +
            QString::number( price_ );
    painter->drawText(QRectF( x_ + OFFSET, y_ + OFFSET, TEXT_WIDTH, TEXT_HEIGHT ),
                      Qt::AlignLeft, text );

    // players
    for ( int i(0); i < visitors_.size(); ++i ){
        painter->drawPixmap( x_ + OFFSET + i*RADIUS, y_ + 8 * OFFSET,
                            visitors_.at(i)->getAvatar().scaledToWidth( 2*RADIUS ));
    }
}

void SquareWidget::setPosition( int x, int y ){
    x_ = x;
    y_ = y;
}

void SquareWidget::addVisitor( PlayerWidget* visitor ){
    visitors_.push_back( visitor );
    this->update( this->boundingRect() );
}

void SquareWidget::removeVisitor( int visitor ){
    for( int i(0); i < visitors_.size(); ++i ){
        if( visitors_.at(i)->getId() == visitor ){
            visitors_.removeAt(i);
            update( boundingRect() );
            break;
        }
    }
}

void SquareWidget::setOwner( char owner ){
    owner_ = owner;
    update( boundingRect() );
}

void SquareWidget::setPrice( int price ){
    price_ = price;
    update( boundingRect() );
}

void SquareWidget::setShack( int shack ){
    shack_ = shack;
    update( boundingRect() );
}


void SquareWidget::changeColor( QColor color )
{
   color_ = color;
   update( boundingRect() );

}
