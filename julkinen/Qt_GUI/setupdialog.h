// Setup dialog for the game
//

#ifndef SETUPDIALOG_H
#define SETUPDIALOG_H

#include <QtWidgets/QDialog>
#include <QString>
#include "gameinterface.h"
#include "namewidget.h"

namespace Ui {
class SetupDialog;
}

class SetupDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SetupDialog( UIInterface* uiInt, QWidget *parent = 0, GameInterface* game = 0 );
    ~SetupDialog();


public slots:
    void getInitFile();
    void startGame();
    void addPlayer(QString name);

signals:
    void closed();

private:
    Ui::SetupDialog *ui;
    UIInterface* uiInt_;
    GameInterface* game_;
    int nextNameBox_;
    int numberOfPlayers_;
    QList<NameWidget*> nameBoxes_;
    SetupDialog& operator= ( const SetupDialog& other );
    SetupDialog( const SetupDialog& other );
    void enableNameBox();
};

#endif // SETUPDIALOG_H
