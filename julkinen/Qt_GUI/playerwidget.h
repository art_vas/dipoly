// Player on the screen

#ifndef PLAYERWIDGET_H
#define PLAYERWIDGET_H

#include <QtWidgets/QLabel>


class PlayerWidget: public QWidget
{
public:
    PlayerWidget( int id, std::string name, int money );
    virtual ~PlayerWidget();
    int getId() const;
    QPixmap getAvatar() const;
    void setMoney( int money );

private:
    int id_;
    QPixmap avatar_;
    QLabel* money_;
    PlayerWidget& operator= ( const PlayerWidget& other );
    PlayerWidget( const PlayerWidget& other );
};

#endif // PLAYERWIDGET_H
