#include "setupdialog.h"
#include "ui_setupdialog.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QLineEdit>
#include <QVBoxLayout>

#include "initreader.hh"
// Change the path to InitReader if needed

namespace {
static const int MAX_PLAYER = 5;
static const int MIN_PLAYER = 2;
static const QString ERR_MIN_PLAYERS = "Minimum number of players is two.";
static const QString TOOLTIP_NAMEBOX = "Type the name and press 'Enter'.";
}

SetupDialog::~SetupDialog() {
    delete ui;
}


SetupDialog::SetupDialog( UIInterface* uiInt, QWidget *parent, GameInterface *game ):
   QDialog( parent ), uiInt_( uiInt ), game_( game ), nextNameBox_( 0 ), numberOfPlayers_(0)
{
    ui = new Ui::SetupDialog;
    ui->setupUi( this );
    ui->start->setEnabled(false);

    // create text boxes for player names
    for( int i(1); i <= MAX_PLAYER; ++i){
        NameWidget* temp = new NameWidget(QString::number(i));
        nameBoxes_.push_back(temp);
        ui->players->layout()->addWidget(temp);
        temp->setEnabled(false);
        temp->setToolTip(TOOLTIP_NAMEBOX);
    }

    connect( ui->loadBoard, SIGNAL(clicked()), this, SLOT(getInitFile()) );
    connect( ui->start, SIGNAL(clicked()), this, SLOT(startGame()) );
    for( int i(0); i < MAX_PLAYER; ++i){
        connect(nameBoxes_.at( i ), SIGNAL(name(QString)), this, SLOT(addPlayer(QString)));
    }
}

void SetupDialog::getInitFile(){
    QString fileName = QFileDialog::getOpenFileName( this, tr( "Open File" ),
                                                     "", tr( "Files (*.txt)" ) );
    ui->fileNameBox->setText( fileName );
    InitReader* reader = new InitReader();

    try {
        reader->readFile( ui->fileNameBox->text().toStdString() );
    }
    catch (FileError const& e){
        QMessageBox msgBox;
        msgBox.setText( QString::fromStdString( e.description() ) );
        msgBox.exec();
    }


    game_->initGame( reader, uiInt_ );
    nextNameBox_ = 0;
    enableNameBox();
    ui->start->setEnabled(true);

}

void SetupDialog::addPlayer(QString name){
    if ( game_->addPlayer( name.toStdString() ) ){
        ++numberOfPlayers_;
        nameBoxes_.at(nextNameBox_- 1)->freeze();
        if ( nextNameBox_ < MAX_PLAYER ){
            enableNameBox();
        }
        else {
            ui->start->setFocus();
        }
    }
}

void SetupDialog::startGame(){
    if ( numberOfPlayers_ >= MIN_PLAYER ){
        game_->startGame();
        emit closed();
        this->close();
    }
    else {
        QMessageBox msgBox;
        msgBox.setText( ERR_MIN_PLAYERS );
        msgBox.exec();
    }
}

void SetupDialog::enableNameBox(){
    nameBoxes_.at( nextNameBox_ )->setEnabled( true );
    nameBoxes_.at( nextNameBox_ )->setFoc();
    ++ nextNameBox_;
}

