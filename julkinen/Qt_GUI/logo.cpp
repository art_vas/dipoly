#include "logo.h"
#include <QPainter>

namespace {
static const int WIDTH = 10;
static const int HEIGHT = 10;
}

QRectF Logo::boundingRect() const {
    return QRectF( 0, 0, WIDTH, HEIGHT );
}

QPainterPath Logo::shape() const {
    QPainterPath path;
    path.addRect( 0, 0, WIDTH, HEIGHT );
    return path;
}

void Logo::paint( QPainter *painter,
                         const QStyleOptionGraphicsItem*, QWidget* ){

    painter->drawImage(x_-65, y_+25 ,image_);
}
