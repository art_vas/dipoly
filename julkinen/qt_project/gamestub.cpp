// this in a very simple example on how to implement the gameinterface
// your own implementation uses the classes etc  from your design
// to implement the tasks.

#include "gamestub.hh"
#include "../Qt_GUI/uiinterface.h"
#include "../Qt_GUI/initexception.hh"
#include "../Qt_GUI/die.hh"
#include <map>

namespace
{
   typedef std::map< CommandDefs::CommandType, std::string > CmdToStr;
   CmdToStr converter;

}

enum UIInterface::SquareColor strAsColor( std::string colorName )
{
   if( colorName == "RED" ) { return UIInterface::RED; }
   else if( colorName == "YELLOW" ) { return UIInterface::YELLOW; }
   else if( colorName == "BLACK" ) { return UIInterface::BLACK; }
   else if( colorName == "GRAY" ) { return UIInterface::GRAY; }
   else if( colorName == "GREEN" ) { return UIInterface::GREEN; }
   else if( colorName == "WHITE" ) { return UIInterface::WHITE; }
   else if( colorName == "PINK" ) { return UIInterface::PINK; }
   else if( colorName == "BLUE" ) { return UIInterface::BLUE; }
   return UIInterface::DEFAULT;
}


GameStub::GameStub(): ui_( 0 ), die_( 0 ), playerNames_(), streets_(), players_( 0 )
{
   //
   converter[ CommandDefs::BRIBE ] = "bribe";
   converter[ CommandDefs::BUILD ] = "build";
   converter[ CommandDefs::BUY ] = "buy";
   converter[ CommandDefs::NEXT ] = "next";
   converter[ CommandDefs::PUSH ] = "push";
   converter[ CommandDefs::INVALID ] = "invalid";
   converter[ CommandDefs::QUIT ] = "quit";
}

GameStub::~GameStub()
{
   if( die_ != 0 ){ delete die_; }
   // Ui is deleted elsewhere
}

void GameStub::initGame( InitReader* init, UIInterface* myUI )
{
   try
   {

      ui_ = myUI;
      streets_ =  init->getStreets();
      for( InitReader::Streets::iterator it = streets_.begin(); it != streets_.end(); ++it )
      {
         Utility::Street st = *it;
         ui_->debugMessage( "adding square " + st.name );
         ui_->addSquare( st.name, st.id, st.shackMax, ' ', st.price );

         // setting the color of the street
         ui_->changeColor( st.id, strAsColor( st.color ) );
      }
      ui_->message( "Name of the game board is " + init->boardName() );

      // An example on how to use the die class

      ui_->message( "Lets throw some die!" );
      die_ = new Die( init->dieSeed() );
      for( unsigned i(0); i < 10; ++i )
      {
         ui_->rollDice( die_->cast() );
      }
      players_ = 0;
      // in your own application you move the player according to the result

   }
   // if init file has errors, catch them & inform ui
   catch( InitException& ie )
   {
      ui_->error( "ERROR: " + ie.description() );
   }
   ui_->draw();
}

void GameStub::startGame()
{
   if( ui_ == 0 ){ return; }

}

// this is where you process the commands, call the
// other objects etc. accordingly (e.g. when a turn ends and AI players
// turn begins, you'll give the control to AI
bool GameStub::processCommand( CommandDefs::Command cmd )
{
   if( ui_ != 0 )
   {
      ui_->message( "received command: " + converter[cmd.type] );


      if( cmd.type == CommandDefs::BUY )
      {
         // using "insane" values for players and squares (does not crash the game)
         ui_->changeMoney( 6, 100 );
         ui_->changeOwner( 22, 1 );
      }
   }
   return true;
}

// Adds a new player, create your own player here and also
// inform the user interface about the newly created players
bool GameStub::addPlayer( std::string playerName, PlayerType  )
{
   if( ui_ == 0 ){ return false; }
   players_++;
   // creates the player in the ui also
   ui_->createPlayer( players_, playerName, players_*1000 );
   // adds the player to the game board in the UI
   ui_->addPlayer( 1, players_ );

   return true;
}
