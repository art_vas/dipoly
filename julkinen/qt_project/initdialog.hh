#ifndef INITDIALOG_HH
#define INITDIALOG_HH

#include <QDialog>

namespace Ui {
class InitDialog;
}

class GameInterface;
class UIInterface;

class InitDialog : public QDialog
{
   Q_OBJECT
   
public:
   explicit InitDialog( UIInterface* uiInt,  GameInterface* game, QWidget *parent = 0 );
   ~InitDialog();

   
private slots:
   void on_selectFilepushButton_clicked();

   void on_startPushButton_clicked();

   void on_addPlayerPushButton_clicked();

private:

   InitDialog( const InitDialog& );
   InitDialog& operator =( const InitDialog& );
   Ui::InitDialog *ui;
   GameInterface* game_;
   UIInterface* uiInt_;

};

#endif // INITDIALOG_HH
