#include "initdialog.hh"
#include "ui_initdialog.h"
#include <QMessageBox>
#include <QFileDialog>

#include "../Qt_GUI/gameinterface.h"
#include "../Qt_GUI/uiinterface.h"
#include "../Qt_GUI/initreader.hh"

namespace {
static const int MAX_PLAYER = 5;
static const int MIN_PLAYER = 2;
static const QString ERR_MIN_PLAYERS = "Minimum number of players is two.";
static const QString ERR_MAX_PLAYERS = "Maximum number of players is five.";
}


InitDialog::InitDialog(UIInterface* uiInt, GameInterface* game, QWidget *parent ) :
   QDialog(parent),
   ui(new Ui::InitDialog), game_( game ), uiInt_( uiInt )
{
   ui->setupUi(this);
   ui->PlayerTypeComboBox->addItem( "Human" );
   ui->PlayerTypeComboBox->addItem( "Basic AI" );
   ui->PlayerTypeComboBox->addItem( "Own AI" );
   ui->PlayerTypeComboBox->addItem( "Unspecified" );
}

InitDialog::~InitDialog()
{
   delete ui;
}

void InitDialog::on_selectFilepushButton_clicked()
{

   QString fileName = QFileDialog::getOpenFileName( this, tr( "Open File" ),
                                                    "", tr( "Files (*.txt)" ) );
   ui->FileNameLineEdit->setText( fileName );
   InitReader* reader = new InitReader();

   try
   {
       reader->readFile( fileName.toStdString() );
   }
   catch( FileError const& e )
   {
       QMessageBox msgBox;
       msgBox.setText( QString::fromStdString( e.description() ) );
       msgBox.exec();
   }
   game_->initGame( reader, uiInt_ );
   ui->addPlayerPushButton->setEnabled( true );
}



void InitDialog::on_startPushButton_clicked()
{
   if ( ui->namesListWidget->count() >= MIN_PLAYER ){
       game_->startGame();
       this->close();
   }
   else {
       QMessageBox msgBox;
       msgBox.setText( ERR_MIN_PLAYERS );
       msgBox.exec();
   }
}

void InitDialog::on_addPlayerPushButton_clicked()
{
   if( ui->namesListWidget->count() >= MAX_PLAYER )
   {
      QMessageBox msgBox;
      msgBox.setText( ERR_MAX_PLAYERS );
      msgBox.exec();

   }

   QString name = ui->PlayerNamelineEdit->text();
   if( name.size() > 2 )
   {
      GameInterface::PlayerType pType = GameInterface::UNSPECIFIED;
      if( ui->PlayerTypeComboBox->currentIndex() == 0 ){ pType = GameInterface::HUMAN; }
      else if( ui->PlayerTypeComboBox->currentIndex() == 1 ){ pType = GameInterface::BASIC_AI; }
      else if( ui->PlayerTypeComboBox->currentIndex() == 2 ){ pType = GameInterface::OWN_AI; }
      else{ pType = GameInterface::UNSPECIFIED; }
      ui->namesListWidget->addItem( name + " " + ui->PlayerTypeComboBox->currentText() );
      game_->addPlayer( name.toStdString(), pType );
   }
}


