// a simple example to show how to implement an interface
// Your job is to implement the actual game behavior behind this
// interface.
// ATTENTION! Your implementation should have the classes, virtual functions,
// etc. as required. Do not copy this as an of how to maintain data etc.

#ifndef GAMESTUB_HH
#define GAMESTUB_HH

#include "../Qt_GUI/gameinterface.h"
#include "../Qt_GUI/initreader.hh"
#include <vector>

// forward declaration of Die
class Die;

class GameStub: public GameInterface
{
public:
   GameStub();


   virtual ~GameStub();


   /* initializes the game by giving the game a pointer to
     * initReader (which has opened and read an init file.
     *
     * If initGame is recalled (called again), you'll need to
     * build a new game, remove everything from the old one
     * and initialize new using the information from initReader.
     */
   virtual void initGame( InitReader* init, UIInterface* myUI );

   /* starts the game, if called again when a game is running
     * restarts the game from the beginning (start setup defined
     * in int file)
     */
   virtual void startGame();

   // Adds a player to the game and
   // returns true if the player is added successfully or
   // false if errors occured. In the later case the game
   // must report the errors to UI with UIInterface::error().
   virtual bool addPlayer( std::string playerName, PlayerType pType = HUMAN );

   // Process a command and return true or false for
   // success or failure correspondingly. Report the
   // errors or messages to UI with UIInterface::error().
   virtual bool processCommand( CommandDefs::Command cmd );
private:

   GameStub( const GameStub& );
   GameStub& operator =( const GameStub& );

   UIInterface* ui_;
   Die* die_;
   std::vector< std::string > playerNames_;
   InitReader::Streets streets_;
   int players_;
};

#endif // GAMESTUB_HH
