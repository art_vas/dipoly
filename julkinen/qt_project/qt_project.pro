#-------------------------------------------------
#
# Project created by QtCreator 2013-02-21T12:56:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt_project
TEMPLATE = app


SOURCES += \
    ../Qt_GUI/squarewidget.cpp \
    ../Qt_GUI/setupdialog.cpp \
    ../Qt_GUI/playerwidget.cpp \
    ../Qt_GUI/namewidget.cpp \
    ../Qt_GUI/mainwindow.cpp \
    ../Qt_GUI/main.cpp \
    initdialog.cpp \
    gamestub.cpp \
    ../Qt_GUI/utility.cc \
    ../Qt_GUI/initreader.cc \
    ../Qt_GUI/chopper.cc \
    ../Qt_GUI/logo.cpp \
    ../Qt_GUI/die.cc

HEADERS  += \
    ../Qt-interfaces/uiinterface.h \
    ../Qt-interfaces/gameinterface.h \
    ../Qt-interfaces/CommandDefs.h \
    ../Qt_GUI/uiinterface.h \
    ../Qt_GUI/squarewidget.h \
    ../Qt_GUI/setupdialog.h \
    ../Qt_GUI/playerwidget.h \
    ../Qt_GUI/namewidget.h \
    ../Qt_GUI/mainwindow.h \
    ../Qt_GUI/gameinterface.h \
    ../Qt_GUI/CommandDefs.h \
    initdialog.hh \
    gamestub.hh \
    ../Qt_GUI/utility.hh \
    ../Qt_GUI/initreader.hh \
    ../Qt_GUI/initexception.hh \
    ../Qt_GUI/chopper.hh \
    ../Qt_GUI/logo.h \
    ../Qt_GUI/die.hh

FORMS    += \
    ../Qt_GUI/setupdialog.ui \
    ../Qt_GUI/mainwindow.ui \
    initdialog.ui

OTHER_FILES += \
    ../Qt_GUI/icon/player5.ico \
    ../Qt_GUI/icon/player4.ico \
    ../Qt_GUI/icon/player3.ico \
    ../Qt_GUI/icon/player2.ico \
    ../Qt_GUI/icon/player1.ico \
    ../Qt_GUI/icon/money.ico
