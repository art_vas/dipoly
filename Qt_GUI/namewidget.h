#ifndef NAMEWIDGET_H
#define NAMEWIDGET_H

#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

class NameWidget : public QWidget
{
    Q_OBJECT
public:
    explicit NameWidget( QString id, QWidget *parent = 0 );
    ~NameWidget(){}
    QString text() const { return name_->text();}
    void freeze();
    
signals:
    void name(QString);
    
public slots:
    void endEditing();
    void setFoc();
    
private:
    QLabel* id_;
    QLineEdit* name_;
    NameWidget& operator= ( const NameWidget& other );
    NameWidget( const NameWidget& other );
};

#endif // NAMEWIDGET_H
