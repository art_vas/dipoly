#ifndef COMMANDDEFS_H
#define COMMANDDEFS_H

namespace CommandDefs
{
   enum CommandType{BUILD, BUY, BRIBE, BOARD, NEXT, PUSH, QUIT, INVALID};
   struct Command
   {
      Command(): type( INVALID ), pushParameter(0){}
      Command( CommandType cmdtype, int pushParam ): type( cmdtype ), pushParameter(pushParam){}
      Command( CommandType cmdtype ): type( cmdtype ), pushParameter(0){}
      CommandType type;
      int pushParameter;
   };

}

#endif // COMMANDDEFS_H
