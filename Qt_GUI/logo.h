#ifndef LOGO_H
#define LOGO_H

#include <QtWidgets/QGraphicsItem>
#include <QtWidgets/QGraphicsEllipseItem>


class Logo : public QGraphicsItem
{
public:
    Logo(): x_(0), y_(0), image_("icon/logo.png") {}
    ~Logo(){}
    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint( QPainter *painter, const QStyleOptionGraphicsItem *item,
                QWidget *widget );

private:
    int x_;
    int y_;
    QImage image_;
};

#endif // LOGO_H
