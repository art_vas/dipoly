#include <QtWidgets/QApplication>
#include "mainwindow.h"
#include "dipolyboard.hh"


#include <iostream>
#include <cstdlib>
#include <vector>

using std::string;
using std::endl;
using std::cout;

int main(int argc, char** argv )
{
    QApplication a(argc, argv);
    DipolyBoard* game = new DipolyBoard();
    MainWindow w(game);
    return a.exec();
}
