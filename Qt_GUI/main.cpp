// starting point of the application
// replace the gamestubb with your own class
//

#include <QApplication>
#include "mainwindow.h"
#include "../qt_project/gamestub.hh"

int main( int argc, char *argv[] )
{
    QApplication a( argc, argv );

    // create your own game object here
    // (the class that implements the gameInterface)
    GameStub* game = new GameStub();

    MainWindow w( game );
    return a.exec();
}
