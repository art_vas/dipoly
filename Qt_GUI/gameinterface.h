// You implement this interface, see an example in gamestub.hh
// on how to start the implementation. (syntax for interface implementation)
//
// Your complete implementation naturally contains more than a single
// class. The gameinterface implementation calls the other objects
// for their services etc. when needed.

#ifndef GAMEINTERFACE_H
#define GAMEINTERFACE_H

#include <string>
#include "CommandDefs.h"

class InitReader;
class UIInterface;

class GameInterface
{
public:

   enum PlayerType{ UNSPECIFIED, HUMAN, BASIC_AI, OWN_AI };

   virtual ~GameInterface(){}


   /* initializes the game by giving the game a pointer to
     * initReader (which has opened and read an init file.
     *
     * If initGame is recalled (called again), you'll need to
     * build a new game, remove everything from the old one
     * and initialize new using the information from initReader.
     */
   virtual void initGame( InitReader* init, UIInterface* myUI ) = 0;

   /* starts the game, if called again when a game is running
     * restarts the game from the beginning (start setup defined
     * in int file)
     */
   virtual void startGame() = 0;

   // Adds a player to the game and
   // returns true if the player is added successfully or
   // false if errors occured. In the later case the game
   // must report the errors to UI with UIInterface::error().
   virtual bool addPlayer( std::string playerName, PlayerType pType = HUMAN ) = 0;

   // Process a command and return true or false for
   // success or failure correspondingly. Report the
   // errors or messages to UI with UIInterface::error().
   virtual bool processCommand( CommandDefs::Command cmd ) = 0;
private:

};

#endif // GAMEINTERFACE_H
