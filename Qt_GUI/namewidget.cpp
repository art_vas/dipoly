#include "namewidget.h"
#include <QtWidgets/QHBoxLayout>

NameWidget::NameWidget( QString id , QWidget *parent ) :
    QWidget( parent ), id_( new QLabel( id ) ), name_( new QLineEdit() )
{
    QHBoxLayout* layout = new QHBoxLayout( this );
    layout->addWidget( id_ );
    layout->addWidget( name_ );
    this->setLayout( layout );
    connect( name_, SIGNAL( returnPressed() ), this, SLOT( endEditing() ) );
}


void NameWidget::endEditing(){
    emit name( name_->text() );
}

void NameWidget::setFoc(){
    name_->setFocus();
}

void NameWidget::freeze(){
    name_->setFrame( false );
    name_->setDisabled( true );
}
