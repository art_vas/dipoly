#-------------------------------------------------
#
# Project created by QtCreator 2013-02-21T12:56:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt_project
TEMPLATE = app


SOURCES += \
    julkinen/Qt_GUI/squarewidget.cpp \
    julkinen/Qt_GUI/setupdialog.cpp \
    julkinen/Qt_GUI/playerwidget.cpp \
    julkinen/Qt_GUI/namewidget.cpp \
    julkinen/Qt_GUI/mainwindow.cpp \
#    julkinen/Qt_GUI/main.cpp \
    proj/main.cpp \
    julkinen/qt_project/initdialog.cpp \
#    julkinen/qt_project/gamestub.cpp \
    proj/game.cpp \
    julkinen/Qt_GUI/utility.cc \
    julkinen/Qt_GUI/initreader.cc \
    julkinen/Qt_GUI/chopper.cc \
    julkinen/Qt_GUI/logo.cpp \
    proj/player.cc \
    proj/human.cpp \
    proj/ai.cpp \
    proj/square.cc \
    julkinen/Qt_GUI/die.cc \
    proj/prison.cpp \
    proj/street.cc \
    proj/card.cpp

HEADERS  += \
    julkinen/Qt-interfaces/uiinterface.h \
    julkinen/Qt-interfaces/gameinterface.h \
    julkinen/Qt-interfaces/CommandDefs.h \
    julkinen/Qt_GUI/uiinterface.h \
    julkinen/Qt_GUI/squarewidget.h \
    julkinen/Qt_GUI/setupdialog.h \
    julkinen/Qt_GUI/playerwidget.h \
    julkinen/Qt_GUI/namewidget.h \
    julkinen/Qt_GUI/mainwindow.h \
    julkinen/Qt_GUI/gameinterface.h \
    julkinen/Qt_GUI/CommandDefs.h \
    julkinen/qt_project/initdialog.hh \
#    julkinen/qt_project/gamestub.hh \
    proj/game.hh \
    julkinen/Qt_GUI/utility.hh \
    julkinen/Qt_GUI/initreader.hh \
    julkinen/Qt_GUI/initexception.hh \
    julkinen/Qt_GUI/chopper.hh \
    julkinen/Qt_GUI/logo.h \
    proj/game.hh \
    proj/player.hh \
    proj/human.h \
    proj/ai.h \
    proj/square.hh \
    julkinen/Qt_GUI/die.hh \
    proj/prison.h \
    proj/street.hh \
    proj/card.h \
    proj/constants.h

FORMS    += \
    julkinen/Qt_GUI/setupdialog.ui \
    julkinen/Qt_GUI/mainwindow.ui \
    julkinen/qt_project/initdialog.ui

OTHER_FILES += \
    julkinen/Qt_GUI/icon/player5.ico \
    julkinen/Qt_GUI/icon/player4.ico \
    julkinen/Qt_GUI/icon/player3.ico \
    julkinen/Qt_GUI/icon/player2.ico \
    julkinen/Qt_GUI/icon/player1.ico \
    julkinen/Qt_GUI/icon/money.ico
