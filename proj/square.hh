#ifndef SQUARE_HH
#define SQUARE_HH

#include <string>
#include "player.hh"
#include "../julkinen/Qt_GUI/uiinterface.h"

class Player;


/*! \brief The basic class describing board squares.
 *
 * Implements functionality common to all squares on board. The game
 * contains a list of Cards the game will operate on.
 * Provides virtual methods for more specific features.
 *
 * The interation with cards on board is performed via virtual Action function to abstract
 * from underlying cards more specific type(Street, Restaurant, Prison etc.)
 */

class Square{

public:

    enum PlayerType{ UNSPECIFIED, HUMAN, BASIC_AI, OWN_AI };

    Square();
    Square(std::string input_name, int input_ID, int input_Price);
    Player* getOwner();
    int setOwner(Player *target);
    virtual std::string getName();
    int getPrice();
    virtual int getID();
    std::string getType();
    virtual int Action(Player *curPlayer, UIInterface *ui_);
    int getHeight();
    int getNumberOfShakes();
    virtual std::string getColor();
    virtual int getShakePrice();
    virtual int Upgrade();
    int resetOwner(UIInterface *ui_);

protected:

    std::string name;
    int ID;
    int Price;
    Player* owner;
    std::string type;
    std::string Color;
    int curHeight;
    int NumberOfShakes;

};

#endif // SQUARE_HH
