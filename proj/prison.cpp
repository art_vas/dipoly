#include "prison.h"
#include <QDebug>

Prison::Prison()
{
}

Prison::Prison(int id, int Price, std::string type, std::string input_name){
    this->ID = id;
    this->Price = Price;
    this->type = type;
    this->name = input_name;
}

int Prison::Action(Player *curPlayer, UIInterface *ui_){
    if(curPlayer->isPrisoned() == true){
        return 3;
    }
    else{

        return 0;
    }
    ui_->draw();
}


std::string Prison::getName(){
    return this->name;
}
