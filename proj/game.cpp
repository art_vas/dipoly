#include "game.hh"
#include "../julkinen/Qt_GUI/uiinterface.h"
#include "../julkinen/Qt_GUI/initexception.hh"
#include <map>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "prison.h"
#include "street.hh"
#include "../julkinen/Qt_GUI/utility.hh"
#include "ai.h"
#include "../julkinen/Qt_GUI/utility.hh"
#include <QDebug>
#include <algorithm>
#include <typeinfo>

namespace
{
   typedef std::map< CommandDefs::CommandType, std::string > CmdToStr;
   CmdToStr converter;

}

Game::Game(): ui_( 0 ), playerNames_(), streets_(), players_( 0 )
{
   //
   converter[ CommandDefs::BRIBE ] = "bribe";
   converter[ CommandDefs::BUILD ] = "build";
   converter[ CommandDefs::BUY ] = "buy";
   converter[ CommandDefs::NEXT ] = "next";
   converter[ CommandDefs::PUSH ] = "push";
   converter[ CommandDefs::INVALID ] = "invalid";
   converter[ CommandDefs::QUIT ] = "quit";

}

Game::~Game()
{
    if( die_ != 0 ){ delete die_; }
       // Ui is deleted elsewhere
}

bool Comparator (Square* i, Square* j) { return (i->getID()<j->getID()); }

void Game::initGame( InitReader* init, UIInterface* myUI )
{
   try
   {

      QMap<int,std::string> color_table;
      color_table.insert(0,"DEFAULT");
      color_table.insert(1,"BLACK");
      color_table.insert(2,"RED");
      color_table.insert(3,"BLUE");
      color_table.insert(4,"YELLOW");
      color_table.insert(5,"GREEN");
      color_table.insert(6,"WHITE");
      color_table.insert(7,"GRAY");
      color_table.insert(8,"PINK");
      ui_ = myUI;
      streets_ =  init->getStreets();
      cards_ = init->getCards(); // vector of structs defined in utility.hh

      /*Here me should destroy everything*/

      current = NULL;
      players_ = 0;
      playerNames_.erase(playerNames_.begin(), playerNames_.end());
      PlayersDB.erase(PlayersDB.begin(),PlayersDB.end());
      SquaresDB.erase(SquaresDB.begin(),SquaresDB.end());
      CardsDB.erase(CardsDB.begin(),CardsDB.end());

      Square* dummy = NULL;
      SquaresDB.push_back(dummy); // Temp fix to keep indexes consistent starting from 1

      for( InitReader::Cards::iterator it = cards_.begin(); it != cards_.end(); ++it ){
          CardUnit* marker = new CardUnit(it->description,it->money,it->move,it->prison);
          CardsDB.push_back(marker);
      }

      std::random_shuffle(CardsDB.begin(),CardsDB.end());

      for( InitReader::Streets::iterator it = streets_.begin(); it != streets_.end(); ++it )
      {
         Utility::Street st = *it;
         ui_->debugMessage( "adding square " + st.name);
         ui_->addSquare( st.name, st.id, st.shackMax, ' ', st.price);
         Square* marker = new Square();
         if(st.stype =="STREET" ){
            marker = new Street(st.name, st.id, st.price, st.shackMax, st.shackPrice, st.color, st.stype );
         }
         else if(st.stype =="RESTAURANT" ){
            marker = new Restaurant(st.name, st.id, st.price, st.shackMax, st.shackPrice, st.color, st.stype );
         }
         else if(st.stype =="NIGHTCLUB" ){
            marker = new NightClub(st.name, st.id, st.price, st.shackMax, st.shackPrice, st.color, st.stype );
         }
         else if(st.stype == "START"){
            marker = new Street(st.name, st.id, st.price, st.shackMax, st.shackPrice, st.color, st.stype );
         }
         else if(st.stype == "CARD"){
            marker = new Card(st.id, st.name, st.stype, st.price, &CardsDB);
         }
         else if(st.stype == "PRISON"){
            marker = new Prison(st.id, st.price, st.stype, st.name);
         }
         else{
             marker = new Street(st.name, st.id, st.price, st.shackMax, st.shackPrice, st.color, st.stype );
         }
         ui_->changeColor(st.id,UIInterface::SquareColor(color_table.key(st.color)));
         SquaresDB.push_back(marker);
      }

      std::sort(SquaresDB.begin()+1,SquaresDB.end(), Comparator);

      ui_->message( "Name of the game board is " + init->boardName() );

      die_ = new Die( init->dieSeed() );
   }
   catch( InitException& ie )
   {
      ui_->error( "ERROR: " + ie.description() );
   }
   ui_->draw();
}

void Game::startGame()
{
   std::random_shuffle(PlayersDB.begin(),PlayersDB.end());
   qDebug() << "Starting game";
   if( ui_ == 0 ){ return; }
   //Make first move
   std::random_shuffle(PlayersDB.begin(),PlayersDB.end());
   current = PlayersDB.begin();
   qDebug() << "Player id:" << (*current)->getId();
   ui_->message(PlayersDB[getCurrentID()-1]->getName() +"  make your move!");

   int g = die_->cast();
   qDebug() << "Dice cast";
   calculate_move(g);
   if ((*current)->getType() == 2){
       (*current)->basic_ai_script(SquaresDB[getCurrentPos()],ui_);
   }
   else if((*current)->getType() == 3){
       (*current)->own_ai_script(SquaresDB[getCurrentPos()],ui_);
   }
}


// this is where you process the commands, call the
// other objects etc. accordingly (e.g. when a turn ends and AI players
// turn begins, you'll give the control to AI
bool Game::processCommand( CommandDefs::Command cmd )
{
   if( ui_ != 0 )
   {
        if(PlayersDB.size() ==  1){
           ui_->announceWinner((PlayersDB[0])->getName());
           return true;
        }

      ui_->message( "received command: " + converter[cmd.type] );

      if(converter[cmd.type] == "next"){

        int g, realID;

        if ((*current) != NULL){

            if ((*current)->isPrisoned()){
                tryLuck();
            }
        }

        if(current == PlayersDB.end())
            current = PlayersDB.begin();
        nextPlayer();
        for(realID =0; PlayersDB[realID]->getId() < getCurrentID()  ; realID++){
            qDebug() << "Cuurent ID: "<<realID << "Of max" << PlayersDB.size();
        }
        ui_->message(PlayersDB[realID]->getName() +"  make your move!");

        if ((*current)->isPrisoned()) return true;

        if ((*current)->getType() == 2){
            g = die_->cast();
            calculate_move(g);
            if ((*current) == NULL){
                qDebug() << "its NULL";
                return false;
            }
            (*current)->basic_ai_script(SquaresDB[getCurrentPos()],ui_);
        }
        else if((*current)->getType() == 3){
            g = die_->cast();
            calculate_move(g);
            if ((*current) == NULL){
                qDebug() << "its NULL";
                return false;
            }
            (*current)->own_ai_script(SquaresDB[getCurrentPos()],ui_);
        }
        else{
            g = die_->cast();
            calculate_move(g);
        }

      }

      else if(converter[cmd.type] == "buy"){
          (*current)->Buy(SquaresDB[getCurrentPos()],ui_);
      }

      else if(converter[cmd.type] == "push"){
            int pushParam = cmd.pushParameter;

           if((pushParam <= players_) && (PlayersDB[pushParam-1]->isPrisoned() == false)){

               (*current)->pushPlayer(PlayersDB[pushParam-1], ui_);
               SquaresDB[PlayersDB[pushParam-1]->getPos()]->Action(PlayersDB[pushParam-1], ui_);
           }
           else if(PlayersDB[pushParam-1]->isPrisoned() == true)
               ui_->message("This player is in a prison");
           else
               ui_->message("There are only " + Utility::int2string(players_) + "players on the board");

      }
      else if(converter[cmd.type] == "bribe"){

          if((SquaresDB[getCurrentPos()])->getType() == "PRISON" && (*current)->isPrisoned()){
              if((*current)->getMoney() < (SquaresDB[getCurrentPos()]->getPrice())){
                  ui_->message("Not enough money for bribing");
              }
              else if(!(*current)->isPrisoned()){
                  ui_->message("You are already free and can do the next turn!");
              }
              else{
                  (*current)->Bribe(SquaresDB[getCurrentID()]->getPrice(), ui_);
                  return false;
              }

          }
          else{
              ui_->message("Nothing to bribe");
          }
      }
      else if(converter[cmd.type] == "build"){
          if(SquaresDB[getCurrentPos()]->getType() == "STREET" || SquaresDB[getCurrentPos()]->getType() == "RESTAURANT"
                  || SquaresDB[getCurrentPos()]->getType() == "NIGHTCLUB"){
            (*current)->Build(SquaresDB[getCurrentPos()],ui_);
          }
      }
      return true;
   }
   return false;
}

// Adds a new player, create your own player here and also
// inform the user interface about the newly created players
bool Game::addPlayer( std::string playerName, PlayerType pType )
{
   if( ui_ == 0 ){ return false; }
   players_++;
   // creates the player in the ui also
   ui_->createPlayer( players_, playerName, players_*1 );
   // adds the player to the game board in the UI
   ui_->addPlayer( 1, players_ );

   // depending on the parameter in configuration file, the code below adds human or AI
   if (pType == HUMAN){
       Human *hummie = new Human(players_,playerName, players_*1000, 1, (Player::PlayerType)pType);
        PlayersDB.push_back(hummie);
        qDebug() << hummie;
   }
   else if(pType == OWN_AI){
        AI *aimmie = new AI(players_,playerName, players_*1000, 1, (Player::PlayerType)pType);
        PlayersDB.push_back(aimmie);
        qDebug() << aimmie;
   }
   else if(pType == BASIC_AI){
        AI *aimmie = new AI(players_,playerName, players_*1000, 1, (Player::PlayerType)pType);
        PlayersDB.push_back(aimmie);
    }
   else{
        Human *hummie = new Human(players_,playerName, players_*1000, 1, (Player::PlayerType)pType);
        PlayersDB.push_front(hummie);
   }

   return true;
}

void Game::restartGame(){
    PlayersDB.erase(PlayersDB.begin(),PlayersDB.end());
    SquaresDB.erase(SquaresDB.begin(),SquaresDB.end());
    CardsDB.erase(CardsDB.begin(),CardsDB.end());
}

//function calculates position on the next turn
//die_val is the number that player gets after throwing the dice
//if die_val is  zero than player is not allowed to move. May be he is in prison.
void Game::calculate_move(int die_val){
    int new_pos =0;
    int cur_pos = 0;

    if (die_val){
            ui_->rollDice( die_val );

            cur_pos = getCurrentPos();
            //
            if(SquaresDB[cur_pos]->getType() == "RESTAURANT"){
                if(die_val%2 == 0){
                    ui_->message("Your are still hungry! Come back and eat more of these soft French loaves, and have some tea!");
                    ui_->removePlayer(getCurrentPos(),getCurrentID());
                    (*current)->setPos(cur_pos);
                    ui_->addPlayer(cur_pos,getCurrentID());
                }
                else{
                    ui_->message("Now you feel better and can go forward");
                    new_pos = (die_val+getCurrentPos())%(SquaresDB.size());
                    int circles = (die_val+getCurrentPos())/(SquaresDB.size());
                    ui_->removePlayer(getCurrentPos(),getCurrentID());
                    ui_->addPlayer(circles+new_pos,getCurrentID());

                    (*current)->setPos(circles+new_pos);
                }
            }

            else{
                new_pos = (die_val+getCurrentPos())%(SquaresDB.size());
                if (new_pos == 0){
                    new_pos = 1;
                }

                int circles = (die_val+getCurrentPos())/(SquaresDB.size());

                if((getCurrentPos() > new_pos) && (SquaresDB[new_pos]->getType() != "PRISON") ){
                    (*current)->setMoney((*current)->getMoney()+(SquaresDB[1]->getPrice()));
                    ui_->changeMoney(getCurrentID(), (*current)->getMoney());
                }

                ui_->removePlayer(getCurrentPos(),getCurrentID());
                ui_->addPlayer(circles+new_pos,getCurrentID());

                (*current)->setPos(circles+new_pos);
            }
        }

    int retval = 0;
    do{
    retval = SquaresDB[getCurrentPos()]->Action(*current,ui_);
    }while (retval == ACT_RECALCULATE);

    if ((*current)->isPrisoned()){
        ui_->removePlayer((*current)->getPos(),(*current)->getId());
        int prison = whereIsPrison();
        if (prison == 0) ui_->message("NO PRISON?");
        (*current)->setPos(prison);
        ui_->addPlayer((*current)->getPos(),(*current)->getId());
    }

    if (retval == ACT_RENT_FAIL) {
        ui_->announceBankrupt((*current)->getName());
        qDebug() << "Player " << (*current)->getId() << "is bankrupt";
        ui_->removePlayer(getCurrentPos(),getCurrentID());
        cleanSquare(*current);
        //delete PlayersDB[(*current)->getId()];
        current++;
        //if(std::distance(PlayersDB.begin(), current) == (PlayersDB.size())){
        if(current == PlayersDB.end()){
            current = PlayersDB.begin();
             PlayersDB.pop_back();
             players_--;
        }
        else{
            PlayersDB.erase(current-1);
            qDebug() << "Now points to player " << (*current)->getId();
            players_--;
        }
    }

    else if(retval == ACT_NIGHTCLUB){
        if(die_val == 1 || die_val == 2){
            ui_->message("Sorry, you are not enough tough for this party!");
            ui_->removePlayer(getCurrentPos(),getCurrentID());
            (*current)->setPos(cur_pos);
            ui_->addPlayer(cur_pos,getCurrentID());

        }
        else{
            ui_->message("Come in");
        }
    }

}

int Game::whereIsPrison(){
    QVector<Square*>::Iterator it;
    for (it=SquaresDB.begin()+1; it != SquaresDB.end(); it++){
        if ((*it)->getType() == "PRISON"){
            return (*it)->getID();
        }
    }
    return 0;
}

int Game::tryLuck(){
        int lucky_chance = die_->cast();
        ui_->message("Trying your luck..");
        ui_->rollDice(lucky_chance);
        if(lucky_chance == 6){
            (*current)->getFromPrison();
            ui_->free((*current)->getId());
            return 1;
        }
        else{
            ui_->message("No luck this time :(");
            return 0;
        }
}

int Game::nextPlayer(){
    current++;
    if (current == PlayersDB.end()){
        current = PlayersDB.begin();
    }

    if(PlayersDB.size() != 1){
        qDebug() <<"The size of array "<< PlayersDB.size();
        ui_->newTurn((*current)->getName(),(*current)->getId(),(*current)->getMoney(),(*current)->isPrisoned());
    }

    return 0;
}

int Game::getCurrentID(){
    return (*current)->getId();
}

int Game::getCurrentPos(){
    return (*current)->getPos();
}

void Game::cleanSquare(Player *Bankrupt){
    QVector<Square*>::iterator cur;
    for(cur = SquaresDB.begin()+1;cur != SquaresDB.end();cur++){

        if((*cur)->getOwner() == Bankrupt){
            (*cur)->resetOwner(ui_);
        }
    }
}
