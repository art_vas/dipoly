#include "card.h"
#include <time.h>
#include <QDebug>
#include <QVector>
#include <stdio.h>
#include <stdlib.h>

#include "game.hh"
#include "constants.h"

Card::Card()
{
    srand((unsigned)time(0));
}

Card::Card(int input_id, std::string input_name, std::string input_type, int input_price,QVector<CardUnit*> *input_acc):
    ID(input_id), name(input_name),type(input_type),Price(input_price), cards_accessor(input_acc), current_card(input_acc->begin()) {

}

int Card::Action(Player *curPlayer,UIInterface* ui_){
    if(ID != 1){
       qDebug() << "Applying card" << QString::fromStdString(this->name);
       pickCard()->apply(curPlayer,ui_);
       if (curPlayer->isPrisoned()) return ACT_OK;
       return ACT_RECALCULATE;
    }
    return 0;
}

int Card::getID(){
    return this->ID;
}

std::string Card::getName(){
    return this->name;
}

CardUnit* Card::pickCard(){
    if (current_card == cards_accessor->end()) current_card = cards_accessor->begin();
    current_card++;
    return *(current_card-1);
}

void CardUnit::print(){

}

void CardUnit::apply(Player *curPlayer,UIInterface* ui_){

    ui_->message("CARD:" + description);
    qDebug() << QString::fromStdString(this->description);

    int new_pos;

    curPlayer->setMoney(curPlayer->getMoney()+price);
    ui_->changeMoney(curPlayer->getId(),curPlayer->getMoney());
    ui_->removePlayer(curPlayer->getPos(),curPlayer->getId());

    if (prison){
        //need to get prison ID
        //curPlayer->setPos(5);
        curPlayer->putToPrison();
        ui_->imprison(curPlayer->getId());
    }

    new_pos = curPlayer->getPos()+move;
    qDebug() << "new pos: " << new_pos;
    if (new_pos > MAX_SQUARE){
        new_pos = new_pos - MAX_SQUARE;
        curPlayer->setMoney(curPlayer->getMoney()+500);
        ui_->changeMoney(curPlayer->getId(),curPlayer->getMoney());
    }

    curPlayer->setPos(new_pos);
    ui_->addPlayer(curPlayer->getPos(),curPlayer->getId());
}

CardUnit::CardUnit(std::string input_descr, int input_price, int input_move, bool int_prison){
    description = input_descr;
    price = input_price;
    move = input_move;
    prison = int_prison;
}
