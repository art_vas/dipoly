#ifndef AI_H
#define AI_H

#include "player.hh"

/*! \brief Child class of Player, defines AI player type.
 *
 * Most of the functionality inherited from Player class, provides implementation for
 * AI player behaviour types. The functions basic_ai_script and own_ai_script are sequences of function
 * calls for the corresponding AI type.
*/

class AI : public Player
{
public:
    AI();
    AI(int input_id,std::string input_name, int input_money,int input_pos,PlayerType input_type);
    virtual void basic_ai_script(Square *target, UIInterface *ui_);
    virtual void own_ai_script(Square *target, UIInterface *ui_);
};

#endif // AI_H
