#include "square.hh"
#include <QDebug>


Square::Square():owner(NULL){

}

Square::Square(std::string input_name, int input_ID, int input_Price):name(input_name),
    ID(input_ID), Price(input_Price), owner(NULL)
{

}

Player* Square::getOwner(){
    return owner;
}

int Square::setOwner(Player *target){
    if (owner == NULL){
        owner = target;
        return 0;
    }

    else return 2;
}

std::string Square::getName(){
    return name;
}

int Square::getPrice(){
    return this->Price;
}

int Square::getShakePrice(){
      return 0;
}

int Square::getID(){
    return this->ID;
}

int Square::Action(Player *curPlayer, UIInterface* ui_){
    ui_->message("Incorrect method");
    ui_->message(curPlayer->getName());
    return 0;

}

std::string Square::getColor(){

    return this->Color;
}

std::string Square::getType(){
    return type;
}

int Square::getHeight(){
    return curHeight;
}

int Square::getNumberOfShakes(){
    return NumberOfShakes;
}

int Square::Upgrade(){
    return 0;
}

int Square::resetOwner(UIInterface *ui_){
    this->owner = NULL;
    this->curHeight = 0;
    ui_->changeOwner(this->ID, 0);
    ui_->changeShacks(this->ID, 0);
    return 0;
}
