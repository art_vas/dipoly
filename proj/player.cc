#include "player.hh"
#include <QDebug>

Player::Player():prisoned(false)
{

}

Player::PlayerType Player::getType(){
    return this->type;
}

void Player::basic_ai_script(Square* target, UIInterface *ui_){
    ui_->message("Incorrect method");
    ui_->message(target->getName());
}

void Player::own_ai_script(Square* target, UIInterface *ui_){
    ui_->message("Incorrect method");
    ui_->message(target->getName());
}

void Player::Build(Square* curSquare, UIInterface *ui_){

    if (curSquare->getOwner() != this){
        ui_->message("You cannot buy the square.");
        return;
    }
    else{
        if(money < curSquare->getShakePrice()){
            ui_->message("You do not have enough money to build a new shack.");
        }
        else if(curSquare->getNumberOfShakes() == curSquare->getHeight()){
            ui_->message("The street already contains the maximum amount of shacks.");
        }
        else{
            curSquare->Upgrade();
            money = money - curSquare->getShakePrice();
            ui_->changeMoney(id, this->money);
            ui_->changeShacks(curSquare->getID(), curSquare->getHeight(),curSquare->getNumberOfShakes());
            ui_->message("You built a new shack on " + curSquare->getName());
            ui_->draw();
        }
    }
}

int Player::Buy(Square* curSquare,UIInterface* ui_){
    if(curSquare->getType() == "PRISON" ){
        ui_->message("You cannot buy the square");
        return 0;
    }
    if(curSquare->getType() == "CARD" ){
        ui_->message("You cannot buy the square");
        return 0;
    }
    if(curSquare->getID() == 1 ){
        ui_->message("You cannot buy the square");
        return 0;
    }
    //int pos = getCurrentPos();
    int price = curSquare->getPrice();

    if ((curSquare->getOwner() == NULL) && (this->money >= price) ){


      curSquare->setOwner(this);
      this->IncrementColor(curSquare->getColor());
      std::string name = curSquare->getName();
      int test_id = (curSquare->getOwner())->getId();

      ui_->changeOwner(curSquare->getID(),test_id);

      this->money -= price;
      ui_->changeMoney(this->id, this->money);
      ui_->message("You bought " + curSquare->getName() );

    }
    else if(this->money < curSquare->getPrice()){
        ui_->message("You do not have enough money to buy " + curSquare->getName() );
    }
 return 0;
}

void Player::pushPlayer(Player* target, UIInterface* ui_){

    if (this->money < 500 && this->money > 0){
        ui_->message("No money bro?! We take all you have");
        ui_->changeMoney(this->id,0);
        return;
    }

    if (this->money <= 0){
        ui_->message("No money to push");
        return;
    }


    int new_pos = target->getPos()-1;
    if (new_pos == 0) new_pos = MAX_SQUARE;

    this->money -= 500;

    ui_->removePlayer(target->getPos(),target->getId());
    ui_->addPlayer(new_pos,target->getId());
    ui_->changeMoney(this->id,this->money);

    target->setPos(new_pos);
}

int Player::getMoney(){
    return this->money;
}

int Player::getPos(){
    return this->pos;
}

int Player::getId(){
    return this->id;
}

int Player::setPos(int input_pos){
    pos = input_pos;
    return 0;
}

int Player::setMoney(int amount){
    this->money =  amount;
    return 0;
}

std::string Player::getName(){
    return name;
}

void Player::IncrementColor(std::string color){

    QMap<std::string,int>::iterator marker;
    marker = ColorTable.find(color);
    if (marker == ColorTable.end()){
        ColorTable.insert(color,1);
    }
    else{
        marker.value()++;
    }
}

int Player::getAmountOf(std::string color){
    QMap<std::string,int>::iterator marker;

    marker = ColorTable.find(color);
    if (marker == ColorTable.end()){
        return 0;
    }
    else{
       return marker.value();
    }
}

int Player::putToPrison(){
    prisoned = true;
    return 0;
}

int Player::getFromPrison(){
    prisoned = false;
    return 0;
}

bool Player::isPrisoned(){
    return prisoned;
}

int Player::Bribe(int price, UIInterface *ui_){
    if (this->money < price){
        ui_->message("You do not have enough money for a bribe.");
        return 0;
    }
    if (this->isPrisoned()){
        setMoney(money - price);
        ui_->changeMoney(id, money);
        getFromPrison();
        ui_->free(id);
        ui_->message("You bribed your way out of prison.");
        return 0;
    }
    else ui_->message("Check your medication. You are not in the prison.");
    return 0;
}
