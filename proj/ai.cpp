#include "ai.h"
#include "QDebug"

//default constructor
AI::AI()
{
}

AI::AI(int input_id, std::string input_name, int input_money, int input_pos, PlayerType input_type){

    this->name = input_name;
    this->money = input_money;
    this->type = input_type;
    this->id = input_id;
    this->pos = input_pos;
}

//basic script. Each turn it tries to buy and upgrade building or bribe if it was caught.
void AI::basic_ai_script(Square* target, UIInterface *ui_){
    if (target->getOwner() == this) this->Build(target, ui_);
    if (target->getType() == "STREET" || target->getType() == "RESTAURANT" || target->getType() == "NIGHTCLUB"){
        this->Buy(target, ui_);
    }
    if (this->isPrisoned()){
        this->Bribe(target->getPrice(),ui_);
    }
}

//It works the same as basic_ai but it tries to make the maximum upgrade on every turn.
void AI::own_ai_script(Square* target, UIInterface *ui_){
    if (target->getType() == "STREET" || target->getType() == "RESTAURANT" || target->getType() == "NIGHTCLUB"){
        this->Buy(target, ui_);
        if(target->getOwner() == this){
            while(target->getHeight() < target->getNumberOfShakes() && this->money >= target->getShakePrice()){
                this->Build(target, ui_);
            }
        }
    }
    if (this->isPrisoned()){
        this->Bribe(target->getPrice(),ui_);
    }
}
