#ifndef PLAYER_HH
#define PLAYER_HH

#include <string>
#include <QMap>
#include "square.hh"
#include "../julkinen/Qt_GUI/uiinterface.h"
#include "constants.h"
#include "../julkinen/Qt_GUI/utility.hh"

class Square;


/*! \brief Basic class describing players. Extended further to Human or AI in corresponding class
 *
 * The player class is the basic abstraction layer for the game. The game process operates on
 * array of pointers to base class. The class implements functionality common to both human and AI
 * for AI it provides virtual functions implementing AI behaviour. Most of the class methods need to
 * drag along the pointer to the user interface.
*/

class Player{

public:

    enum PlayerType{ UNSPECIFIED, HUMAN, BASIC_AI, OWN_AI };

    Player();
    int Buy(Square* curSquare, UIInterface *ui_);
    void Build(Square* curSquare, UIInterface *ui_);
    void pushPlayer(Player *target, UIInterface *ui_);
    int Bribe(int price, UIInterface *ui_);

    std::string getName();
    int getMoney();
    int setMoney(int amount);
    PlayerType getType();
    int getPos();
    int getId();
    int setPos(int input_pos);
    int putToPrison();
    int getFromPrison();
    bool isPrisoned();
    void IncrementColor(std::string color); /*!< The function to add newly bought squares color to players property colors*/
    int getAmountOf(std::string color);     /*!< Returns the number of sqares with given color the player owns. Used in rent calculation*/
    virtual void basic_ai_script(Square *target, UIInterface *ui_);
    virtual void own_ai_script(Square *target, UIInterface *ui_);

protected:

    int id;
    std::string name;
    int money;
    PlayerType type;
    int pos;
    bool prisoned;
    QMap<std::string, int> ColorTable; /*!< Contains the amount of same colored squares belonging to the player*/

};


#endif // PLAYER_HH
