// a simple example to show how to implement an interface
// Your job is to implement the actual game behavior behind this
// interface.
// ATTENTION! Your implementation should have the classes, virtual functions,
// etc. as required. Do not copy this as an of how to maintain data etc.

#ifndef Game_HH
#define Game_HH

#include "../julkinen/Qt_GUI/gameinterface.h"
#include "../julkinen/Qt_GUI/initreader.hh"
#include <QVector>
#include <map>
#include "human.h"
#include "player.hh"
#include "square.hh"
#include "card.h"
#include "constants.h"
#include "julkinen/Qt_GUI/die.hh"

/*! \brief Main game engine class
 *
 * The game instance instantiates the game contents and links the players with game objects like streets, cards, etc.
 * Rolls the dice, moves the players around the borad and calculates outcome of every move.
 */

class Game: public GameInterface
{
public:
   Game();


   virtual ~Game();


   /* initializes the game by giving the game a pointer to
     * initReader (which has opened and read an init file.
     *
     * If initGame is recalled (called again), you'll need to
     * build a new game, remove everything from the old one
     * and initialize new using the information from initReader.
     */
   virtual void initGame( InitReader* init, UIInterface* myUI );

   /* starts the game, if called again when a game is running
     * restarts the game from the beginning (start setup defined
     * in int file)
     */
   virtual void startGame();

   // Adds a player to the game and
   // returns true if the player is added successfully or
   // false if errors occured. In the later case the game
   // must report the errors to UI with UIInterface::error().
   virtual bool addPlayer( std::string playerName, PlayerType pType = HUMAN );

   // Process a command and return true or false for
   // success or failure correspondingly. Report the
   // errors or messages to UI with UIInterface::error().
   virtual bool processCommand( CommandDefs::Command cmd );

   virtual void restartGame();

   int getCurrentID();
   int getCurrentPos();
   int getPos(int player_id);
   int tryLuck();      /*!< The function to roll dice when player is in prison */
   int nextPlayer();   /*!< Switch player*/
   int whereIsPrison(); /*!< Returns prison square id*/

private:

   Game( const Game& );
   Game& operator =( const Game& );

   UIInterface* ui_;
   std::vector< std::string > playerNames_;
   InitReader::Streets streets_;
   InitReader::Cards cards_;
   int players_;
   Die* die_;                           /*!< wrapper function to pick card from the pile*/
   CardUnit* pickCard();
   void calculate_move(int die_val);    /*!< calculate the move outcome based on rolled dice*/
   void cleanSquare(Player *Bankrupt);

   QVector<Player*>::iterator current;  /*!< The game keeps track of current player using an iterator to players list*/
   QVector<Player*> PlayersDB;          /*!< Players taking part in the game. Actual underlying sub class type is not important and hidden by abstraction*/
   QVector<Square*> SquaresDB;          /*!< Cards the game operates on, again actual square types are hidden*/
   QVector<CardUnit*> CardsDB;          /*!< Action cards available in the game*/

};


#endif // Game_HH
