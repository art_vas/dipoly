#ifndef HUMAN_H
#define HUMAN_H

#include "player.hh"
#include "../julkinen/Qt_GUI/gameinterface.h"

/*! \brief Child class of Player, defines features specific to Human player
 *
 * Most of the functionality inherited from Player class.
*/

class Human : public Player
{
public:
    Human();
    Human(int input_id,std::string input_name, int input_money,int input_pos,PlayerType input_type);
    //void IncrementColor(std::string color);
    //int getAmountOf(std::string color);
};

#endif // HUMAN_H
