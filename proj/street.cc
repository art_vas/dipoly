#include "street.hh"
#include "constants.h"
#include "../julkinen/Qt_GUI/utility.hh"
#include <QDebug>

Street::Street(){

}

Street::Street(std::string input_name, int input_ID, int input_Price,
    int input_NumOfSnacks, int input_shakePrice, std::string input_Color, std::string type){
    this->Color = input_Color;
    this->ID = input_ID;
    this->name = input_name;
    this->NumberOfShakes = input_NumOfSnacks;
    this->Price = input_Price;
    this->shakePrice = input_shakePrice;
    this->type = type;
    this->curHeight = 0;

}

std::string Street::getColor(){
    return Color;
}

int Street::Action(Player *curPlayer, UIInterface* ui_){
    if(owner == NULL || owner == curPlayer){
        return ACT_OK;
    }
    int num_of_strts = owner->getAmountOf(Color);
    int rent = num_of_strts*Price/4+(curHeight)*(curHeight)*Price/10;
    ui_->message("Applying rent:" + Utility::int2string(rent));
    if(curPlayer->getMoney() < rent){
        curPlayer->setMoney(curPlayer->getMoney() - rent);
        return ACT_RENT_FAIL;
    }
    else{

       curPlayer->setMoney(curPlayer->getMoney() - rent);
       owner->setMoney(owner->getMoney() + rent);
       ui_->changeMoney(curPlayer->getId(), curPlayer->getMoney() );
       ui_->changeMoney(owner->getId(), owner->getMoney());
    }
    return ACT_OK;
}

std::string Street::getName(){
    return name;
}
int Street::Upgrade(){
    this->curHeight++;
    return 0;
}

int Street::getShakePrice(){
    return this->shakePrice;
}

Restaurant::Restaurant(){

}

Restaurant::Restaurant(std::string input_name, int input_ID, int input_Price,
                       int input_NumOfSnacks, int input_shakePrice, std::string input_Color, std::string type){
                       this->Color = input_Color;
                       this->ID = input_ID;
                       this->name = input_name;
                       this->NumberOfShakes = input_NumOfSnacks;
                       this->Price = input_Price;
                       this->shakePrice = input_shakePrice;
                       this->type = type;
                       this->curHeight = 0;

}

int Restaurant::Action(Player *curPlayer, UIInterface* ui_){

    ui_->message("Now you are in a restaurant, "+ curPlayer->getName() +
                 " and you need to get an  uneven number to move");

    if(owner == NULL || owner == curPlayer){
        return ACT_RESTAURANT;
    }
    int num_of_strts = owner->getAmountOf(Color);
    int rent = num_of_strts*Price/4+(curHeight)*(curHeight)*Price/10;
    ui_->message("Applying rent:" + Utility::int2string(rent));
    if(curPlayer->getMoney() < rent){
        return ACT_RENT_FAIL;
    }
    else{
       curPlayer->setMoney(curPlayer->getMoney() - rent);
       owner->setMoney(owner->getMoney() + rent);
       ui_->changeMoney(curPlayer->getId(), curPlayer->getMoney() );
       ui_->changeMoney(owner->getId(), owner->getMoney());
    }
    return ACT_RESTAURANT;
}

NightClub::NightClub(){

}

NightClub::NightClub(std::string input_name, int input_ID, int input_Price,
                       int input_NumOfSnacks, int input_shakePrice, std::string input_Color, std::string type){
                       this->Color = input_Color;
                       this->ID = input_ID;
                       this->name = input_name;
                       this->NumberOfShakes = input_NumOfSnacks;
                       this->Price = input_Price;
                       this->shakePrice = input_shakePrice;
                       this->type = type;
                       this->curHeight = 0;

}

int NightClub::Action(Player *curPlayer, UIInterface* ui_){

    if(owner == NULL || owner == curPlayer){
        ui_->message("You feel bored, "+ curPlayer->getName() +
                     " and decide to go shuffling to a club but Security in the entrance Stopped you: ");
        return ACT_NIGHTCLUB;
    }
    int num_of_strts = owner->getAmountOf(Color);
    int rent = num_of_strts*Price/4+(curHeight)*(curHeight)*Price/10;
    ui_->message("Applying rent:" + Utility::int2string(rent));
    if(curPlayer->getMoney() < rent){
        return ACT_RENT_FAIL;
    }
    else{

       curPlayer->setMoney(curPlayer->getMoney() - rent);
       owner->setMoney(owner->getMoney() + rent);
       ui_->changeMoney(curPlayer->getId(), curPlayer->getMoney() );
       ui_->changeMoney(owner->getId(), owner->getMoney());
    }
    return ACT_NIGHTCLUB;
}
