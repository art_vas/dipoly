#ifndef PRISON_H
#define PRISON_H

#include <QVector>
#include <QMap>
#include "square.hh"
#include "constants.h"
#include "../julkinen/Qt_GUI/uiinterface.h"

/*! \brief Prison square object
 *
 * Applies the actions per Prison specification.
 */

class Prison : public Square
{
public:
    Prison();
    Prison(int id, int Price, std::string type, std::string input_name);
    virtual int Action(Player *curPlayer, UIInterface *ui_); /*!< For the prison Action is arrest, trial, feeding to penetentiary system*/
    std::string getName();
private:
    int id;
    std::string name;
    int Price;
};

#endif // PRISON_H
