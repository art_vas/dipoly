#ifndef STREET_HH
#define STREET_HH

#include "square.hh"
#include "../julkinen/Qt_GUI/uiinterface.h"

/*! \brief The street card on board
 *
 * The card of a street with possibility to buy it, build shack.
 * virtual Action function performs the calculations of rent on player entry.
 */

class Street : public Square
{
public:

    Street();
    Street(std::string input_name, int input_ID, int input_Price, int input_NumOfSnacks,
           int shakePrice, std::string Color, std::string type);
    virtual std::string getName();
    virtual int Action(Player *curPlayer,UIInterface* ui_); /*!< For the street Action is rent calculation */
    virtual int Upgrade();
    virtual int getShakePrice();
    std::string getColor();

protected:
    std::string Color;
    int shakePrice;
    //std::string name;
    //int ID;
    //int Price;
    //Player* Owner;

};

/*! \brief The street card for Restaurant
 *
 * the Action function in restaurant performs the action per Restaurant specification -
 * hold the player till even dice roll
 */
class Restaurant : public Street
{
public:
    Restaurant();
    Restaurant(std::string input_name, int input_ID, int input_Price, int input_NumOfSnacks,
           int shakePrice, std::string Color, std::string type);
    virtual int Action(Player *curPlayer, UIInterface *ui_); /*!< For the restaurant Action invokes eating chalenge*/
};



/*! \brief The street card for NightClub
 *
 * the Action function in nightclub performs the action per specification -
 * checks the dice roll and returns player back if 1 or 2
 */

class NightClub: public Street
{
public:
    NightClub();
    NightClub(std::string input_name, int input_ID, int input_Price, int input_NumOfSnacks,
           int shakePrice, std::string Color, std::string type);
    virtual int Action(Player *curPlayer, UIInterface *ui_); /*!< For the nightclub Action is face control*/
};

#endif // STREET_HH
