#ifndef CARD_H
#define CARD_H

#include "square.hh"
//#include "game.hh"
#include <QVector>

/*! \brief Helper class to represent cards from pile
 *
 * CardUnit object represents a single card from the pile on the card square.
 * the object is described by the card inscription, and parameters of the actions
 * imposed by it, provides method to apply card to a player.
*/

class CardUnit
{
public:
    CardUnit();
    CardUnit(std::string input_descr, int input_price, int input_move, bool int_prison);
    void print();
    void apply (Player *curPlayer,UIInterface* ui_);

private:
    std::string description;
    std::string Color;
    int price;
    int move;
    bool prison;
};


/*! \brief Card square on the game field.
 *
 * The square contains a link to a vector of CardUinit objects. Defines actions
 * to draw a card from a pile and apply it to a certain player.
*/

class Card : public Square
{
public:
    Card();
    Card(int input_id, std::string input_name, std::string type,
                      int input_price, QVector<CardUnit*> *input_acc);
    virtual int Action(Player *curPlayer, UIInterface *ui_); /*!< A wrapper function to apply card imposed actions to a player */
    virtual int getID();
    std::string getName();
    CardUnit* pickCard();

private:

     int ID;
    std::string name;
    std::string type;
    int Price;
   // int redirect;
    QVector<CardUnit*> *cards_accessor;
    QVector<CardUnit*>::iterator current_card;

};

#endif // CARD_H
