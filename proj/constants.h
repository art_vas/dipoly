#ifndef CONSTANTS_H
#define CONSTANTS_H

/*! Contains constants, and function return codes used in
 * game state machine.
*/

#define MAX_SQUARE 16

#define ACT_OK 0
#define ACT_RECALCULATE 1
#define ACT_RENT_FAIL 2
#define ACT_PRISON_THROWDICE 3
#define ACT_RESTAURANT 4
#define ACT_NIGHTCLUB 5

#endif // CONSTANTS_H
