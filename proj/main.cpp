// starting point of the application
// replace the gamestubb with your own class
//

#include <QApplication>
#include "../julkinen/Qt_GUI/mainwindow.h"
#include "game.hh"

int main( int argc, char *argv[] )
{
    QApplication a( argc, argv );

    // create your own game object here
    // (the class that implements the gameInterface)
    Game* game = new Game();

    MainWindow w( game );

    return a.exec();
}
