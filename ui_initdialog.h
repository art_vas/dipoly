/********************************************************************************
** Form generated from reading UI file 'initdialog.ui'
**
** Created: Mon May 20 15:42:59 2013
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INITDIALOG_H
#define UI_INITDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_InitDialog
{
public:
    QPushButton *selectFilepushButton;
    QLabel *label;
    QComboBox *PlayerTypeComboBox;
    QPushButton *addPlayerPushButton;
    QLineEdit *PlayerNamelineEdit;
    QLineEdit *FileNameLineEdit;
    QPushButton *startPushButton;
    QListWidget *namesListWidget;

    void setupUi(QDialog *InitDialog)
    {
        if (InitDialog->objectName().isEmpty())
            InitDialog->setObjectName(QString::fromUtf8("InitDialog"));
        InitDialog->resize(400, 300);
        selectFilepushButton = new QPushButton(InitDialog);
        selectFilepushButton->setObjectName(QString::fromUtf8("selectFilepushButton"));
        selectFilepushButton->setGeometry(QRect(310, 10, 81, 23));
        label = new QLabel(InitDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(30, 50, 111, 16));
        PlayerTypeComboBox = new QComboBox(InitDialog);
        PlayerTypeComboBox->setObjectName(QString::fromUtf8("PlayerTypeComboBox"));
        PlayerTypeComboBox->setGeometry(QRect(160, 70, 101, 21));
        addPlayerPushButton = new QPushButton(InitDialog);
        addPlayerPushButton->setObjectName(QString::fromUtf8("addPlayerPushButton"));
        addPlayerPushButton->setEnabled(false);
        addPlayerPushButton->setGeometry(QRect(310, 70, 75, 23));
        PlayerNamelineEdit = new QLineEdit(InitDialog);
        PlayerNamelineEdit->setObjectName(QString::fromUtf8("PlayerNamelineEdit"));
        PlayerNamelineEdit->setGeometry(QRect(30, 70, 113, 20));
        FileNameLineEdit = new QLineEdit(InitDialog);
        FileNameLineEdit->setObjectName(QString::fromUtf8("FileNameLineEdit"));
        FileNameLineEdit->setEnabled(false);
        FileNameLineEdit->setGeometry(QRect(30, 10, 261, 21));
        startPushButton = new QPushButton(InitDialog);
        startPushButton->setObjectName(QString::fromUtf8("startPushButton"));
        startPushButton->setGeometry(QRect(310, 270, 75, 23));
        namesListWidget = new QListWidget(InitDialog);
        namesListWidget->setObjectName(QString::fromUtf8("namesListWidget"));
        namesListWidget->setGeometry(QRect(30, 100, 231, 161));

        retranslateUi(InitDialog);

        QMetaObject::connectSlotsByName(InitDialog);
    } // setupUi

    void retranslateUi(QDialog *InitDialog)
    {
        InitDialog->setWindowTitle(QApplication::translate("InitDialog", "Game initialization", 0, QApplication::UnicodeUTF8));
        selectFilepushButton->setText(QApplication::translate("InitDialog", "Select init file", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("InitDialog", "Name of the player", 0, QApplication::UnicodeUTF8));
        addPlayerPushButton->setText(QApplication::translate("InitDialog", "Add player", 0, QApplication::UnicodeUTF8));
        startPushButton->setText(QApplication::translate("InitDialog", "Start game", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class InitDialog: public Ui_InitDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INITDIALOG_H
